
Roctave
=======

Roctave is a Ruby gem attempting to provide functions do design digital filters in Ruby.
Most algorithms are taken from [GNU Octave](https://www.gnu.org/software/octave/).

Installation
------------

```bash
git clone https://gitlab.com/theotime_bollengier/roctave.git
cd roctave
gem build roctave.gemspec
sudo gem install ./roctave-x.x.x.gem
```


Documentation
-------------

You can generate the documentation with [YARD](https://yardoc.org/).

To get `yard` :
```bash
sudo gem install yard
```
Then, in this directory, generate the documentation :
```bash
yard doc -o doc
firefox doc/index.html
```


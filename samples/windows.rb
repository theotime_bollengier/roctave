#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'
require 'gnuplot'

Gnuplot.open do |gp|
	Gnuplot::Plot.new(gp) do |plot|
		plot.terminal 'wxt'
		plot.title "Roctave::Window"
		plot.yrange "[0:1.1]"
		plot.grid

		n = 100
		x = (0...n).to_a

		[:hann, :hamming, :blackman, :nuttall, :blackman_nuttall, :blackman_harris, :gaussian].each do |name|
			y = Roctave::Window.send(name, n)
			plot.data << Gnuplot::DataSet.new([x, y]) { |ds|
				ds.with = "lines"
				ds.title = name.to_s.split('_').collect{|s| s.capitalize}.join(' ')
			}
		end	
	end
end


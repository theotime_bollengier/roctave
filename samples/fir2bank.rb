#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

n = 64
f1 = [0.0, 0.2, 0.2, 1.0]
m1 = [1.0, 1.0, 0.0, 0.0]
f2 = [0.0, 0.2, 0.2, 0.4, 0.4, 1.0]
m2 = [0.0, 0.0, 1.0, 1.0, 0.0, 0.0]
f3 = [0.0, 0.4, 0.4, 0.6, 0.6, 1.0]
m3 = [0.0, 0.0, 1.0, 1.0, 0.0, 0.0]
f4 = [0.0, 0.6, 0.6, 0.8, 0.8, 1.0]
m4 = [0.0, 0.0, 1.0, 1.0, 0.0, 0.0]
f5 = [0.0, 0.8, 0.8, 1.0]
m5 = [0.0, 0.0, 1.0, 1.0]
b1 = Roctave.fir2(n, f1, m1)
b2 = Roctave.fir2(n, f2, m2)
b3 = Roctave.fir2(n, f3, m3)
b4 = Roctave.fir2(n, f4, m4)
b5 = Roctave.fir2(n, f5, m5)

w  = Roctave.linspace(0.0, 1.0, 1024)
h1 = Roctave.dft(b1, 2048).abs[0...1024]
h2 = Roctave.dft(b2, 2048).abs[0...1024]
h3 = Roctave.dft(b3, 2048).abs[0...1024]
h4 = Roctave.dft(b4, 2048).abs[0...1024]
h5 = Roctave.dft(b5, 2048).abs[0...1024]
h = (0...1024).collect{|i| h1[i] + h2[i] + h3[i] + h4[i] + h5[i]}
Roctave.plot(w, h1, w, h2, w, h3, w, h4, w, h5, w, h, grid: true)


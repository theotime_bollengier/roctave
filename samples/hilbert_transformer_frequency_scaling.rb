#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'



n = 33
f = [0, 1]
m = [Complex(0, -1), Complex(0, -1)]

b = Roctave.fir2(n, f, m, :odd_symetry, window: :blackman)

Roctave.stem(b, :filled, grid: true)

r = Roctave.dft(b, 2048)
r = Roctave.fftshift(r)
h = r.abs
p = r.arg
w = Roctave.linspace(-1, 1, 2048)
Roctave.plot([-1, 1], [1, 1], '--;Ideal;', w, h, ';Actual;', grid: true, title: 'Magnitude')
Roctave.plot(w, p, grid: true, title: 'Phase')

=begin
n = [2, order.to_i].max
n += n & 1
grid = 1024
mgridl = Roctave.linspace(0, Math::PI, grid + 1).collect{|i| Complex(0.0, i)}
mgridr = mgridl[1...-1].reverse.conj
mgrid = mgridl + mgridr
b = Roctave.idft(mgrid)
mid = (n+1) / 2.0
b = (b[-(mid.floor) .. -1] + b[0 ... mid.ceil]).collect{|e| e.real}
window = Roctave.blackman(n+1)
b = b.collect.with_index{|e, i| e*window[i]}

indexes = (-n/2..n/2).to_a
Roctave.stem(indexes, b, :filled, grid: true, title: "FIR type III order #{n}")

r = Roctave.dft(b, 2048)
h = r[0...1024].abs
p = r[0...1024].arg
w = Roctave.linspace(0, 1, 1024)
Roctave.plot([0, Math::PI], '--;Ideal;', w, h, ";FIR type III order #{n};", grid: true, title: 'Magnitude')
Roctave.plot(w, p, grid: true, title: 'Phase')
=end



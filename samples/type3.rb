#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

n = 32

b1 = Roctave.fir_differentiator(n, window: :blackman)
ns = (-n/2..n/2).to_a
Roctave.stem(ns, b1, :filled, grid: true)

h1 = Roctave.dft(b1, 2048).abs[0...1024]
w = Roctave.linspace(0.0, 1.0, 1024)

bb = b1[n/2+1..-1]
h2 = (0...1024).collect do |i|
	acc = 0.0
	bb.each.with_index do |e, j|
		acc += -e*Math.sin((i*Math::PI/1024)*(j+1))
	end
	b1[n/2] + 2.0*acc
end

Roctave.plot(w, h1, ';real;', w, h2, ';test;', grid: true)


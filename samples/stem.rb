#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

t = Roctave.linspace(0, 2*Math::PI, 50)
y = t.collect{|x| Math.exp(0.3*x) * Math.sin(3*x)}
Roctave.stem(t, y, :legend, 'Coucou', t, y.reverse, ';plop;', :filled, grid: true)


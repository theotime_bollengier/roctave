#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'


n = 8

###

b1 = Roctave.fir_differentiator(n, window: :blackman)

Roctave.stem((-n/2..n/2).to_a, b1, :filled, grid: true, title: "Differentiator")

r1 = Roctave.dft(b1, 2048)
h1 = r1[0...1024].abs
p1 = r1[0...1024].arg
p1 = Roctave.unwrap(p1)
w1 = Roctave.linspace(0, 1, 1024)
Roctave.plot([0, Math::PI], '--;Ideal;', w1, h1, ";Filter;", grid: true, title: 'Differentiator magnitude')
Roctave.plot(w1, p1, grid: true, title: 'Differentiator phase')

###

b2 = Roctave.finite_difference_coefficients((-n/2 .. n/2), 1)

Roctave.stem((-n/2..n/2).to_a, b2, :filled, grid: true, title: "Finite difference")

r2 = Roctave.dft(b2, 2048)
h2 = r2[0...1024].abs
p2 = r2[0...1024].arg
p2 = Roctave.unwrap(p2)
w2 = Roctave.linspace(0, 1, 1024)
Roctave.plot([0, Math::PI], '--;Ideal;', w2, h2, ";Filter;", grid: true, title: 'Finite difference magnitude')
Roctave.plot(w2, p2, grid: true, title: 'Finite difference phase')

###

n = 16
d = 3
b2 = Roctave.finite_difference_coefficients((-n/2 .. n/2), d)
pp b2.reverse

Roctave.stem((-n/2..n/2).to_a, b2, :filled, grid: true, title: "Finite difference length #{n+1} derivative order #{d}")

r2 = Roctave.dft(b2, 2048)
h2 = r2[0...1024].abs
p2 = r2[0...1024].arg
p2 = Roctave.unwrap(p2)
w2 = Roctave.linspace(0, 1, 1024)
Roctave.plot(w2, w2.collect{|e| (Math::PI*e)**d}, '--;Ideal;', w2, h2, ";Filter;", grid: true, title: 'Finite difference magnitude length #{n+1} derivative order #{d}')
Roctave.plot(w2, p2, grid: true, title: 'Finite difference phase length #{n+1} derivative order #{d}')



#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

fs = 600
fn = fs/2.0
t = Roctave.linspace(0, 1, fs)
s = t.collect{|t| 1*Math.cos(2*Math::PI*100*t + 0*Math::PI/3) + 0.8*Math.cos(2*Math::PI*150*t + 1*Math::PI/3) + 0.6*Math.cos(2*Math::PI*200*t + 2*Math::PI/3)}
#Roctave.plot(t, s, grid: true)

st = Time.now
dft = Roctave.fftshift(Roctave.dft(s, 2048, window: :nuttall, normalize: true).abs)
et = Time.now
puts "Took #{(et - st).round(3)} s to compute #{dft.length}-points DFT"
dft.collect!{|e| 20.0*Math.log10(e)}
f = Roctave.linspace(-fn, fn, dft.length)
#Roctave.plot(f, dft, grid: true, title: 'DFT', xlabel: 'frequency', ylabel: 'Amplitude')
Roctave.plot(f, dft, grid: true, title: 'DFT', xlabel: 'frequency', ylabel: 'dB', ylim: [-150, 0])

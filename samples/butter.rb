#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

#b, a = Roctave.butter(12, [0.3, 0.7], :stop)
b, a = Roctave.butter(12, 0.75, :low)
pp b #.collect{|v| v.round(5)}
pp a #.collect{|v| v.round(5)}

Roctave.freqz(b, a, :magnitude, :phase, :group_delay)
Roctave.zplane(b, a)


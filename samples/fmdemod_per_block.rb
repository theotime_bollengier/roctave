#!/usr/bin/env ruby

require 'roctave'

abort "Usage: #{File.basename(__FILE__)} <raw_cu8_file_name> [frequency_shift]" if ARGV.length < 1 or ARGV.length > 2

show_filters = false

fs = 2.048e6
fc = 104.45e6


reader = Roctave::CU8FileReader.new ARGV[0]

shifter = Roctave::FreqShifter.new freq: ARGV[1].to_f, fs: fs if ARGV.length > 1

bbdecimator = Roctave::FirFilter.fir1(254, 95e3*2/fs)
bbdecimator.decimation_factor = 8
bbdecimator.freqz(:magnitude, nb_points: 2048, fs: fs) if show_filters

differentiator = Roctave::FirFilter.differentiator(32)
differentiator.freqz(:magnitude, nb_points: 2048, fs: fs/8) if show_filters

adecimator = Roctave::FirFilter.low_pass(512, 15e3*2/256e3)
adecimator.decimation_factor = 8
adecimator.freqz(:magnitude, nb_points: 2048, fs: fs/8) if show_filters

tau = 50e-6
ts = 8*8/fs
b = [1 - Math.exp(-ts/tau)]
a = [1, -Math.exp(-ts/tau)]
h, w = Roctave.freqz(b, a, nb_points: 1024)
h.collect!{|v| v.abs}
w.collect!{|v| v/w.last}
n = 2048
deemphasis = Roctave::FirFilter.fir2(n, w, h, :odd_symmetry)
deemphasis.freqz(:magnitude, nb_points: 2048, fs: fs/8/8) if show_filters

outfile = File.open('output.f32le', 'wb')

bar_length = 80 - 3
start = Time.now
loop do
	progression = (reader.normalized_position*bar_length).round
	STDOUT.write "\r[#{'='*progression}>#{'-'*(bar_length-progression)}] #{(reader.position / fs).round(1)}s  "; STDOUT.flush

	if2MHz_complex = reader.read fs/8
	if2MHz_complex = shifter.shift(if2MHz_complex) if ARGV.length > 1
	baseband256kHz_complex = bbdecimator.filter(if2MHz_complex)
	baseband_diff_complex, baseband_delayed_complex = differentiator.filter_with_delay(baseband256kHz_complex)
	demodulated = baseband_diff_complex.length.times.collect do |i|
		re = baseband_delayed_complex[i].real
		im = baseband_delayed_complex[i].imag
		(baseband_diff_complex[i].imag * re - baseband_diff_complex[i].real * im) / (re*re + im*im + 0.00001)
	end
	audioemph = adecimator.filter(demodulated)
	audio = deemphasis.filter(audioemph)
	outfile.write audio.collect{|v| v/4}.pack('f*')
	break if reader.eof?
end
progression = (reader.normalized_position*bar_length).round
puts "\r[#{'='*progression}>#{'-'*(bar_length-progression)}] #{(reader.position / fs).round(1)}s  "
puts "Processing took #{(Time.now - start).round(1)}s"

outfile.close
reader.close

system "ffmpeg -v error -ar #{(fs/8/8).round} -ac 1 -f f32le -i output.f32le -y output.wav"

system "mpv output.wav"


#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

n = 64
fc = 0.25
width = 0.1

b = Roctave.fir_low_pass(n, fc - width/2)
Roctave.stem(b, grid: true)
w = Roctave.linspace(0.0, 1.0, 1024)
h = Roctave.dft(b, 2048).abs[0...1024]
Roctave.plot(w, h, title: 'Low-pass', xlim: [-0.1, 1.1], ylim: [-0.1, 1.1], grid: true)

b = Roctave.fir_high_pass(n, fc + width/2)
Roctave.stem(b)
w = Roctave.linspace(0.0, 1.0, 1024)
h = Roctave.dft(b, 2048).abs[0...1024]
Roctave.plot(w, h, title: 'High-pass', xlim: [-0.1, 1.1], ylim: [-0.1, 1.1], grid: true)

b = Roctave.fir_band_stop(n, fc, width, window: :hamming)
Roctave.stem(b, :filled, grid: true)
w = Roctave.linspace(0.0, 1.0, 1024)
h = Roctave.dft(b, 2048).abs[0...1024]
Roctave.plot(w, h, [fc - width / 2.0, fc + width / 2.0], [0.5, 0.5], title: 'Band-stop', xlim: [-0.1, 1.1], ylim: [-0.1, 1.1], grid: true)

b = Roctave.fir_band_pass(n & ~1, fc, width, window: :hamming)
Roctave.stem(b, :filled, grid: true, title: "Band-pass even #{b.length - 1}")
w = Roctave.linspace(0.0, 1.0, 1024)
h = Roctave.dft(b, 2048).abs[0...1024]
Roctave.plot(w, h, [fc - width / 2.0, fc + width / 2.0], [0.5, 0.5], title: "Band-pass even #{b.length - 1}", xlim: [-0.1, 1.1], ylim: [-0.1, 1.1], grid: true)

b = Roctave.fir_band_pass((n & ~1) | 1, fc, width, window: :hamming)
Roctave.stem(b, :filled, grid: true, title: "Band-pass odd #{b.length - 1}")
w = Roctave.linspace(0.0, 1.0, 1024)
h = Roctave.dft(b, 2048).abs[0...1024]
Roctave.plot(w, h, [fc - width / 2.0, fc + width / 2.0], [0.5, 0.5], title: "Band-pass odd #{b.length - 1}", xlim: [-0.1, 1.1], ylim: [-0.1, 1.1], grid: true)

b = Roctave.fir2(n, [0, fc-width/2, fc-width/2, fc+width/2, fc+width/2, 1], [0, 0, 1, 1, 0, 0])
Roctave.stem(b, :filled, grid: true, title: "Band-pass fir2 #{b.length - 1}")
w = Roctave.linspace(0.0, 1.0, 1024)
h = Roctave.dft(b, 2048).abs[0...1024]
Roctave.plot(w, h, [fc - width / 2.0, fc + width / 2.0], [0.5, 0.5], title: "Band-pass fir2 #{b.length - 1}", xlim: [-0.1, 1.1], ylim: [-0.1, 1.1], grid: true)


#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

# t = Roctave.linspace(0, 1)
# s1 = t.collect{|e| Math.sin(2*Math::PI*2*e + 0*Math::PI/2)}
# s2 = t.collect{|e| Math.sin(2*Math::PI*2*e + 1*Math::PI/2)}
# s3 = t.collect{|e| Math.sin(2*Math::PI*2*e + 2*Math::PI/2)}
# s4 = t.collect{|e| Math.sin(2*Math::PI*2*e + 3*Math::PI/2)}
# Roctave.plot(t, s1, '-+;{/Symbol f} = 0;', t, s2, '--o;{/Symbol f} = {/Symbol p} / 2;', t, s3, ':*;{/Symbol f} = 2 {/Symbol p} / 2;', t, s4, '-..;{/Symbol f} = 3 {/Symbol p} / 2;', grid: true, title: 'y = sin(x + {/Symbol f})', xlabel: 'Time t^2 h^{titi} t_2 t_{titi} Z@^{en haut}_{en bas}  escape\\\_underscore', ylabel: 'Amplitude', terminal: 'pngcairo transparent truecolor rounded enhanced font "Sans,10" size 1280,720', output: 'titi.png')
# #Roctave.plot(t, s1, '+-;{/Symbol f} = 0;', t, s2, '--o;{/Symbol f} = {/Symbol p} / 2;', t, s3, ':*;{/Symbol f} = 2 {/Symbol p} / 2;', t, s4, '-..;{/Symbol f} = 3 {/Symbol p} / 2;', grid: true, title: 'y = sin(x + {/Symbol f})', xlabel: 'Time t^2 h^{titi} t_2 t_{titi} Z@^{en haut}_{en bas}  escape\\\_underscore', ylabel: 'Amplitude', terminal: "wxt enhanced persist font \"Times New Roman,13\" size 1280,720")
# Roctave.plot(t, s1, '+-;{/Symbol f} = 0;', t, s2, '--o;{/Symbol f} = {/Symbol p} / 2;', t, s3, ':*;{/Symbol f} = 2 {/Symbol p} / 2;', t, s4, '-..;{/Symbol f} = 3 {/Symbol p} / 2;', grid: true, title: 'y = sin(x + {/Symbol f})', xlabel: 'Time t^2 h^{titi} t_2 t_{titi} Z@^{en haut}_{en bas}  escape\\\_underscore', ylabel: 'Amplitude', terminal: "wxt enhanced persist font \"Sans,13\" size 1280,720")
# Roctave.plot(
# 	t, s1, 
# 	:color,
# 	'orange',
# 	:dashtype,
# 	'-_',
# 	:pointtype,
# 	:square,
# 	:empty,
# 	:pointsize,
# 	2,
# 	:linewidth,
# 	2,
# 	grid: true, title: 'y = sin(x + {/Symbol f})', xlabel: 'Time t^2 h^{titi} t_2 t_{titi} Z@^{en haut}_{en bas}  escape\\\_underscore', ylabel: 'Amplitude', terminal: "wxt enhanced persist size 1280,720"
# )

t = Roctave.linspace(0, 1, 1000)
s1 = t.collect{|e| Math.sin(2*Math::PI*2*e + 0*Math::PI/2)}
s2 = t.collect{|e| Math.sin(2*Math::PI*2*e + 1*Math::PI/2)}
s3 = t.collect{|e| Math.sin(2*Math::PI*2*e + 2*Math::PI/2)}
s4 = t.collect{|e| Math.sin(2*Math::PI*2*e + 3*Math::PI/2)}
Roctave.plot(
	t, s1, ';{/Symbol f} = 0;', 
	t, s2, ';{/Symbol f} = {/Symbol p} / 2;', 
	t, s3, ';{/Symbol f} = 2 {/Symbol p} / 2;', 
	t, s4, ';{/Symbol f} = 3 {/Symbol p} / 2;', 
	grid: true, 
	title: 'y = sin(x + {/Symbol f})', 
	xlabel: 'Time t^2 h^{titi} t_2 t_{titi} Z@^{en haut}_{en bas}  escape\\\_underscore', 
	ylabel: 'Amplitude', 
	terminal: "wxt enhanced persist size 854,480"
)


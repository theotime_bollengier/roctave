#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

fs = 8192
fn = fs/2.0
t = Roctave.linspace(0, 1, fs)
s = t.collect{|t| 1*Math.cos(2*Math::PI*100*t + 0*Math::PI/3) + 0.8*Math.cos(2*Math::PI*150*t + 1*Math::PI/3) + 0.6*Math.cos(2*Math::PI*200*t + 2*Math::PI/3)}

n = 4096

st = Time.now
fft = Roctave.fftshift(Roctave.fft(s, n, window: :nuttall, normalize: true).abs)
et = Time.now
puts "Took #{(et - st).round(3)} s to compute #{fft.length}-points FFT"
ffttime = et - st


st = Time.now
dft = Roctave.fftshift(Roctave.dft(s, n, window: :nuttall, normalize: true).abs)
et = Time.now
puts "Took #{(et - st).round(3)} s to compute #{dft.length}-points DFT"
dfttime = et - st
puts "FFT acceleration factor: #{(dfttime / ffttime).round(3)}"

fft.collect!{|e| 20.0*Math.log10(e)}
dft.collect!{|e| 20.0*Math.log10(e)}

dftf = Roctave.linspace(-fn, fn, dft.length)
fftf = Roctave.linspace(-fn, fn, fft.length)

#Roctave.plot(f, dft, grid: true, title: 'DFT', xlabel: 'frequency', ylabel: 'Amplitude')
Roctave.plot(fftf, fft, ";FFT;", dftf, dft, "--;DFT;", grid: true, title: 'DFT', xlabel: 'frequency', ylabel: 'dB', ylim: [-150, 0])

st = Time.now
si = Roctave.idft(Roctave.dft(s));
et = Time.now
puts "Took #{(et - st).round(3)} s to compute forward and inverse #{si.length}-points DFT"
Roctave.plot(t, s, ';original;', Roctave.linspace(0, 1, si.length), si, '--;reconstructed;')


#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

b, a = Roctave.cheby2(5, -0.1, (0.4 .. 0.6), :stop)
pp b
pp a

Roctave.freqz(b, a, :magnitude)
Roctave.zplane(b, a)
exit
b, a = Roctave.cheby1(12, -0.01, (0.4 .. 0.6), :stop)
pp b
pp a

Roctave.freqz(b, a, :magnitude)
Roctave.zplane(b, a)

plot_args = []
(1..10).each do |i|
	b, a = Roctave.cheby1(i, -0.2, 0.5)
	h, w = Roctave.freqz(b, a)
	plot_args << w.collect{|v| v/Math::PI}
	plot_args << h.abs
	plot_args << ";n = #{i};"
end

Roctave.plot(*plot_args, grid: true)

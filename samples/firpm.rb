#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

## Low pass type I
n = 60 
f = [(0 .. 0.2), (0.25 .. 0.5), (0.55 .. 1)]
a = [0, 1, 0]
b = Roctave.firpm(n, f, a, :even_symmetry, 16)
exit

# ## Low pass type I
# n = 32 
# f = [(0 .. 0.45), (0.55 .. 0.7), (0.75 .. 1)]
# a = [1, 0, 0.5]
# w = [1, 1, 20]
# b = Roctave.firpm(n, f, a, w, :even_symmetry)

# ## Low pass type II
# n = 11 
# f = [(0 .. 0.3), (0.7 .. 1)]
# a = [1, 0]
# w = [2, 1]
# b = Roctave.firls(n, f, a, w, :even_symmetry)

# ## Differentiator type III
# n = 10 
# f = [0.05, 0.75]
# a = [f]
# w = [Roctave.linspace(f.first, f.last, 16).collect{|v| 1/v}]
# b = Roctave.firls(n, f, a, w, 16, :odd_symmetry)

# ## Hilbert transformer type IV
# n = 11 
# f = [1.0/8, 7.0/8]
# a = [1]
# w = [1]
# b = Roctave.firls(n, f, a, w, :odd_symmetry)



r = Roctave.dft(b, 2048)
h = r[0..1024].abs
p = r[0..1024].arg
p = Roctave.unwrap(p)
w = Roctave.linspace(0, 1, 1025)

# if (n & 1) == 0 then
# 	Roctave.stem((-n/2..n/2).to_a, b, :filled, grid: true)
# else
# 	Roctave.stem((n+1).times.collect{|i| i - (n+1)/2.0 + 0.5}, b, :filled, grid: true)
# end
# Roctave.plot(w, p, grid: true, title: 'Phase')
# Roctave.plot(w, h.collect{|v| 10*Math.log10(v)}, xlabel: 'Normalized frequency', ylabel: 'Magnitude response (dB)', grid: true, title: 'Magnitude')
Roctave.plot([0, 0.3], [1, 1], '-b', [0.7, 1], [0, 0], '-b', w, h, 'r', title: 'Magnitude response', xlim: (0 .. 1), grid: true)





exit
n = 4
f = [(0 .. 0.4), (0.6 .. 1)]
a = [1, 0]
b = Roctave.firls(n, f, a, 1)
exit

n = 32
grid = 8
f = [[0.2, 0.3], [0.4, 0.6], [0.7, 0.8]]
a = [
	0.5,
	Roctave.linspace(0, Math::PI, 64).collect{|t| 1 - Math.sin(t)},
	(0.5 .. 0)
]

b = Roctave.firls(n, f, a, grid)


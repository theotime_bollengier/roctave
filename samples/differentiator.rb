#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

# H(e^(jw)) = j.w, for -pi <= w <= pi
# h(n) = 0 when n == 0, else (-1)^n / n

n = 14


b = Roctave.fir_differentiator(n, window: :blackman)
Roctave.freqz(b, :magnitude, :phase, :group_delay)
exit

Roctave.stem((-n/2..n/2).to_a, b, :filled, grid: true, title: "FIR type III order #{n}")

r1 = Roctave.dft(b, 2048)
h1 = r1[0...1024].abs
p1 = r1[0...1024].arg
p1 = Roctave.unwrap(p1)
w1 = Roctave.linspace(0, 1, 1024)
Roctave.plot([0, Math::PI], '--;Ideal;', w1, h1, ";FIR type III order #{n};", grid: true, title: 'Magnitude')
Roctave.plot(w1, p1, grid: true, title: 'Phase')

###

fc = 1.0/2
b = Roctave.fir_differentiator_low_pass(n, fc, window: :hamming)

Roctave.stem((-n/2..n/2).to_a, b, :filled, grid: true, title: "FIR type III order #{n} \\\"lowpass\\\"")

r2 = Roctave.dft(b, 2048)
h2 = r2[0...1024].abs
p2 = r2[0...1024].arg
p2 = Roctave.unwrap(p2)
w2 = Roctave.linspace(0, 1, 1024)
Roctave.plot([0, fc, 2*fc, 1], [0, Math::PI * fc, 0, 0], '--;Ideal;', w2, h2, ";FIR type III order #{n} \"lowpass\";", grid: true, title: 'Magnitude')
Roctave.plot(w2, p2, grid: true, title: 'Phase')


Roctave.plot(
	[0, Math::PI], '--;Ideal 1;', 
	[0, fc, 2*fc, 1], [0, Math::PI * fc, 0, 0], '--;Ideal 2;', 
	w1, h1, ";1 FIR type III order #{n};",
	w2, h2, ";2 FIR type III order #{n} \"lowpass\";", 
	grid: true, title: 'Magnitude'
)


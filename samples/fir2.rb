#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

f = [0.0, 0.3, 0.3, 0.6, 0.6, 1.0]
m = [0, 0, 1, 0.5, 0, 0]
b = Roctave.fir2(60, f, m)

h = Roctave.dft(b, 2048).abs[0...1024]
p = Roctave.dft(b, 2048).arg[0...1024]
w = Roctave.linspace(0.0, 1.0, 1024)
Roctave.plot(f, m, ';Ideal;', w, h, '-;fir2;', title: 'Magnitude')
#Roctave.plot(w, p, title: 'Phase')


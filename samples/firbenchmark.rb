#!/usr/bin/env ruby

require 'roctave'


class FIR
	attr_reader :n, :b
	attr_accessor :decimation

	def initialize (b)
		@b = b.collect{|v| v.to_f}
		@blen = @b.length
		@n = @blen - 1
		@b_size = 2**Math.log2(@blen).ceil
		@buf = Array.new(@b_size){0.0}
		@bsizemask = @b_size - 1
		@index = 0
		@cnt = 0
		@decimation = 1
	end


	def plot (fs = nil)
		Roctave.freqz(@b, :magnitude, fs: fs, nb_points: 2048)
	end


	def stem
		Roctave.stem(@b, grid: true)
	end


	# @param x [Float] sample to process
	# @return [Array<Float>] the processed sample, and the delayed original sample
	def process_sample (x)
		res = nil
		@buf[@index] = x
		if (@cnt % @decimation) == 0 then
			@cnt = 0
			start_index = (@b_size + @index - @blen + 1) & @bsizemask
			res = 0.0
			(0...@blen).each do |i|
				res += @b[i] * @buf[start_index]
				start_index = (start_index + 1) & @bsizemask
			end
		end
		@cnt += 1
		@index = (@index + 1) & @bsizemask
		return res
	end
end


n = 256
b = Roctave.fir1(n, 0.234)
fr = FIR.new(b)
fc = Roctave::FirFilter.new b

nb_samples = 2**22
fs = nb_samples.to_f
ts = 1 / fs

t = nb_samples.times.collect{|i| i*ts}
x = nb_samples.times.collect{rand*2-1}

if false then
st = Time.now
yr = x.collect{|v| fr.process_sample(v)}
t_ruby = (Time.now - st)
else
	t_ruby = nb_samples*8.921/2**18
end
puts "Ruby processing took #{t_ruby.round(3)} s"

st = Time.now
ycs = x.collect{|v| fc.filter(v)}
t_cs = (Time.now - st)
puts "C s/s processing took #{t_cs.round(3)} s, (#{t_ruby / t_cs})"

fc.reset!

st = Time.now
ycb = fc.filter(x)
t_cb = (Time.now - st)
puts "C b/b processing took #{t_cb.round(3)} s, (#{t_ruby / t_cb})"

#sserror = nb_samples.times.collect{|i| (yr[i] - ycs[i]).abs}.max
#bberror = nb_samples.times.collect{|i| (yr[i] - ycb[i]).abs}.max
#puts "s/s error: #{sserror}, b/b error: #{bberror}"

#Roctave.plot(t, yr, ';Ruby;', t, ycs, ';C per sample;', t, ycb, ';C per block;')


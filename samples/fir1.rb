#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

n = 128
fc = 0.5

b1 = Roctave.fir1(n, [0.4, 0.6, 0.8], :high)

h1 = Roctave.dft(b1, 2048).abs[0...1024]
w = Roctave.linspace(0.0, 1.0, 1024)
Roctave.plot(w, h1, [0.5], [fc], '*', grid: true, ylim: [0, 1.4])


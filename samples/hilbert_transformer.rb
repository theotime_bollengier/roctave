#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

# H(e^(jw)) = -j when 0 <= w < pi, j when -pi <= w < 0
# h(n) = 0 when n == 0, else (1 - (-1)^n) / (pi.n)

n = 64

b = Roctave.fir_hilbert(n, window: :nuttall)

Roctave.stem((-n/2..n/2).to_a, b, :filled, grid: true, title: "FIR type III order #{n}")

r = Roctave.dft(b, 2048)
r = Roctave.fftshift(r)
h = r.abs
p = r.arg
w = Roctave.linspace(-1, 1, 2048)
Roctave.plot([-1, 1], [1, 1], '--;Ideal;', w, h, ";FIR type III order #{n};", grid: true, title: 'Magnitude')
Roctave.plot(w, p, grid: true, title: 'Phase')

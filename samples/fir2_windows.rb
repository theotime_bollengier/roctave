#!/usr/bin/env ruby

require_relative '../lib/roctave.rb'

f = [
 	0.00000, 0.01000, 0.02000, 0.03000, 0.04000, 0.05000, 0.06000, 0.07000, 0.08000, 0.09000, 0.10000, 0.11000, 0.12000,
 	0.13000, 0.14000, 0.15000, 0.16000, 0.17000, 0.18000, 0.20000, 0.38000, 0.40000, 0.55000, 0.56200, 0.58500, 0.60000,
 	0.78000, 0.79000, 0.80000, 0.81000, 0.82000, 0.83000, 0.84000, 0.85000, 0.86000, 0.87000, 0.88000, 0.89000, 0.90000,
 	0.91000, 0.92000, 0.93000, 0.94000, 0.95000, 0.96000, 0.97000, 0.98000, 0.99000, 1.00000
]                                                                                                                       
m = [                                                                                                                   
	0.50000, 0.61350, 0.70225, 0.74692, 0.73776, 0.67678, 0.57725, 0.46089, 0.35305, 0.27725, 0.25000, 0.27725, 0.35305,
	0.46089, 0.57725, 0.67678, 0.73776, 0.74692, 0.70225, 0.50000, 2.30000, 1.00000, 1.00000, 0.20000, 0.20000, 1.00000,
	1.00000, 0.99380, 0.92000, 0.84980, 0.78320, 0.72020, 0.66080, 0.60500, 0.55280, 0.50420, 0.45920, 0.41780, 0.38000,
	0.34580, 0.31520, 0.28820, 0.26480, 0.24500, 0.22880, 0.21620, 0.20720, 0.20180, 0.20000
]

n = 128
hamming          = Roctave.dft(Roctave.fir2(n, f, m, window: :hamming),          2048).abs[0...1024]
hann             = Roctave.dft(Roctave.fir2(n, f, m, window: :hann),             2048).abs[0...1024]
blackman         = Roctave.dft(Roctave.fir2(n, f, m, window: :blackman),         2048).abs[0...1024]
nuttall          = Roctave.dft(Roctave.fir2(n, f, m, window: :nuttall),          2048).abs[0...1024]
blackman_nuttall = Roctave.dft(Roctave.fir2(n, f, m, window: :blackman_nuttall), 2048).abs[0...1024]
blackman_harris  = Roctave.dft(Roctave.fir2(n, f, m, window: :blackman_harris),  2048).abs[0...1024]
gaussian         = Roctave.dft(Roctave.fir2(n, f, m, window: :gaussian),         2048).abs[0...1024]

w = Roctave.linspace(0.0, 1.0, 1024)
Roctave.plot(f, m, ';Ideal;', w, hamming, ';hamming;', w, hann, ';hann;', w, blackman, ';blackman;', w, nuttall, ';nuttall;', w, blackman_nuttall, ';blackman nuttall;', w, blackman_harris, ';blackman harris;', w, gaussian, ';gaussian;', title: 'Magnitude')


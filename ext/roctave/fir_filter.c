/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of Roctave
 * 
 * Roctave is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Roctave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Roctave. If not, see <https://www.gnu.org/licenses/>.
 */


#include <ruby.h>
#include "roctave.h"
#include "fir_filter.h"

VALUE c_FIR;

static ID id_real;
static ID id_imag;


struct fir_filter {
	double *b;
	double *state;
	double *stateim;
	int     nb_coefficients;
	int     state_length;
	int     index_mask;
	int     index;
	int     counter;             // used for decimation
	int     max_counter;         // used decimation/upsampling factor
	int     decimator_interpolator; // 0 simple filter, 1 decimator, 2 interpolator 
	void    (*push_one_sample_func)(struct fir_filter*, VALUE);
	VALUE   (*filter_one_sample_func)(struct fir_filter*, VALUE, VALUE*, double);
	VALUE   zero; // Float or Complex ruby zero, used for interpolator
};


#define FIR_FILTER_DECIMATOR_FLAG 1
#define FIR_FILTER_INTERPOLATOR_FLAG 2
#define FIR_FILTER_IS_DECIMATOR(flt) ((flt)->decimator_interpolator & FIR_FILTER_DECIMATOR_FLAG)
#define FIR_FILTER_IS_INTERPOLATOR(flt) ((flt)->decimator_interpolator & FIR_FILTER_INTERPOLATOR_FLAG)
#define FIR_FILTER_IS_REGULAR(flt)   ((flt)->decimator_interpolator == 0)


static void fir_filter_mark(void *p)
{
	struct fir_filter *flt = (struct fir_filter*)p;
	rb_gc_mark(flt->zero);
}


static void fir_filter_free(void *p)
{
	struct fir_filter *flt = (struct fir_filter*)p;

	if (flt->b)
		free(flt->b);

	if (flt->state)
		free(flt->state);

	if (flt->stateim)
		free(flt->stateim);

	xfree(flt);
}


static size_t fir_filter_size(const void* data)
{
	const struct fir_filter *flt;
	size_t res;

	flt = (const struct fir_filter*)data;
	res = sizeof(struct fir_filter);
	res += flt->nb_coefficients*sizeof(double);
	res += flt->state_length*sizeof(double);
	if (flt->stateim)
		res += flt->state_length*sizeof(double);

	return res;
}


static const rb_data_type_t fir_filter_type = {
	.wrap_struct_name = "roctave_fir_filter_struct",
	.function = {
		.dmark = fir_filter_mark,
		.dfree = fir_filter_free,
		.dsize = fir_filter_size,
	},
	.data = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


/* @return [Integer] the filter order
 */
static VALUE fir_filter_order(VALUE self)
{
	struct fir_filter *flt;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	return INT2NUM(flt->nb_coefficients - 1);
}


/* @return [Integer] number of coefficients (order + 1)
 */
static VALUE fir_filter_nb_coefficients(VALUE self)
{
	struct fir_filter *flt;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	return INT2NUM(flt->nb_coefficients);
}


/* @param index [Integer] the coefficient index
 * @param coef  [Float] the coefficient value
 * @return [Float, nil] the coefficient if the index is valid, or nil otherwise
 */
static VALUE fir_filter_set_coefficient_at(VALUE self, VALUE index, VALUE coef)
{
	struct fir_filter *flt;
	int ind;
	double val;
	VALUE res = Qnil;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	ind = NUM2INT(index);
	if (ind >= 0 && ind < flt->nb_coefficients) {
		val = NUM2DBL(coef);
		flt->b[flt->nb_coefficients - 1 - ind] = val;
		res = DBL2NUM(val);
	}

	return res;
}


/* @param index [Integer] the coefficient index
 * @return [Float] the coefficient at index
 */
static VALUE fir_filter_get_coefficient_at(VALUE self, VALUE index)
{
	struct fir_filter *flt;
	int ind;
	VALUE res = Qnil;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	ind = NUM2INT(index);
	if (ind >= 0 && ind < flt->nb_coefficients)
		res = DBL2NUM(flt->b[flt->nb_coefficients - 1 - ind]);
	else
		res = DBL2NUM(0.0);

	return res;
}


/* This method noes nothing, it is there to be compatible with Roctave::IirFilter
 * @param index [Integer] the coefficient index
 * @param coef  [Float] the denominator coefficient value
 * @return [Float, nil] the denominator coefficient if the index is valid, or nil otherwise
 */
static VALUE fir_filter_set_denominator_at(VALUE self, VALUE index, VALUE coef)
{
	(void)self;
	(void)index;
	(void)coef;

	return Qnil;
}


/* This method returns 1.0 if +index+ = 0, 0.0 otherwise. It is there to be compatible with Roctave::IirFilter
 * @param index [Integer] the denominator coefficient index
 * @return [Float] the denominator coefficient at index
 */
static VALUE fir_filter_get_denominator_at(VALUE self, VALUE index)
{
	int ind;
	VALUE res;

	ind = NUM2INT(index);
	if (ind == 0)
		res = DBL2NUM(1.0);
	else
		res = DBL2NUM(0.0);

	return res;
}


/* Return a copy of the filter's numerator coefficients as an array.
 * @return [Array<Float>] the filter coefficients
 */
static VALUE fir_filter_coefficients(VALUE self)
{
	struct fir_filter *flt;
	VALUE res;
	int i;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);
	
	res = rb_ary_new_capa(flt->nb_coefficients);
	for (i = 0; i < flt->nb_coefficients; i++)
		rb_ary_store(res, i, DBL2NUM(flt->b[flt->nb_coefficients - 1 - i]));

	return res;
}


/* Return a copy of the filter's denominator coefficients as an array, allways [1.0].
 * @return [Array<Float>] the filter coefficients
 */
static VALUE fir_filter_denominator(VALUE self)
{
	VALUE res = rb_ary_new_capa(1);
	rb_ary_store(res, 0, DBL2NUM(1.0));
	return res;
}


static void push_one_sample_complex(struct fir_filter *flt, VALUE sample)
{
	flt->state[flt->index]  =  NUM2DBL(rb_funcallv(sample, id_real, 0, NULL));
	flt->stateim[flt->index] = NUM2DBL(rb_funcallv(sample, id_imag, 0, NULL));
	flt->index = (flt->index + 1) & flt->index_mask;
}


static VALUE filter_one_sample_complex(struct fir_filter *flt, VALUE samp, VALUE *delayed, double post_mult)
{
	const int flt_len = flt->nb_coefficients;
	const int mask = flt->index_mask;
	const int start_index = (flt->state_length + flt->index + 1 - flt_len) & mask;
	double accr = 0.0;
	double acci = 0.0;
	double re, im;
	int i, j;

	flt->state[flt->index]  =  NUM2DBL(rb_funcallv(samp, id_real, 0, NULL));
	flt->stateim[flt->index] = NUM2DBL(rb_funcallv(samp, id_imag, 0, NULL));

	for (i = 0, j = start_index; i < flt_len; i++, j = (j + 1) & mask) {
		accr += flt->b[i] * flt->state[j];
		acci += flt->b[i] * flt->stateim[j];
	}

	flt->index = (flt->index + 1) & mask;

	if (delayed) {
		if (flt_len & 1) {
			re = flt->state[(start_index + flt_len/2) & mask];
			im = flt->stateim[(start_index + flt_len/2) & mask];
		}
		else {
			re = (flt->state[(start_index + flt_len/2) & mask] + flt->state[(start_index + flt_len/2 - 1) & mask]) / 2.0;
			im = (flt->stateim[(start_index + flt_len/2) & mask] + flt->stateim[(start_index + flt_len/2 - 1) & mask]) / 2.0;
		}
		*delayed = rb_complex_raw(DBL2NUM(re * post_mult), DBL2NUM(im * post_mult));
	}

	return rb_complex_raw(DBL2NUM(accr * post_mult), DBL2NUM(acci * post_mult));
}


static void make_accept_complex(struct fir_filter *flt)
{
	if (!flt->stateim)
		flt->stateim = calloc(flt->state_length, sizeof(double));
	if (!flt->stateim)
		rb_raise(rb_eRuntimeError, "Failed to allocate %lu bytes of state storage", flt->state_length*sizeof(double));
	flt->push_one_sample_func = &push_one_sample_complex;
	flt->filter_one_sample_func = &filter_one_sample_complex;
	flt->zero = rb_complex_raw(DBL2NUM(0.0), DBL2NUM(0.0));
}


static void push_one_sample_float(struct fir_filter *flt, VALUE sample)
{
	if (rb_class_of(sample) == rb_cComplex) {
		make_accept_complex(flt);
		push_one_sample_complex(flt, sample);
	}
	else {
		flt->state[flt->index] = NUM2DBL(sample);
		flt->index = (flt->index + 1) & flt->index_mask;
	}
}


/* Not visible from the Ruby API.
 * Process one Float sample, returning a Float sample.
 * If the input sample is a Complex sample ane not a Float one,
 * makes the filter accept Complex sample and returns filter_one_sample_complex().
 * Once the filter has accepted one Complex, it will allways return complex samples.
 */
static VALUE filter_one_sample_float(struct fir_filter *flt, VALUE samp, VALUE *delayed, double post_mult)
{
	const int flt_len = flt->nb_coefficients;
	const int mask = flt->index_mask;
	const int start_index = (flt->state_length + flt->index + 1 - flt_len) & mask;
	double acc = 0.0;
	int i, j;

	if (rb_class_of(samp) == rb_cComplex) {
		make_accept_complex(flt);
		return filter_one_sample_complex(flt, samp, delayed, post_mult);
	}

	flt->state[flt->index] = NUM2DBL(samp);

	for (i = 0, j = start_index; i < flt_len; i++, j = (j + 1) & mask)
		acc += flt->b[i] * flt->state[j];

	flt->index = (flt->index + 1) & mask;

	if (delayed) {
		if (flt_len & 1)
			*delayed = DBL2NUM(flt->state[(start_index + flt_len/2) & mask] * post_mult);
		else
			*delayed = DBL2NUM((flt->state[(start_index + flt_len/2) & mask] + flt->state[(start_index + flt_len/2 - 1) & mask]) * post_mult / 2.0);
	}

	return DBL2NUM(acc * post_mult);
}


/* @param sample [Numeric, Array<Numeric>] a single sample or an array of samples to process
 * @return [Float, Complex, Array<Float, Complex>] a single or an array of processed samples
 */
static VALUE fir_filter_filter(VALUE self, VALUE sample)
{
	struct fir_filter *flt;
	VALUE res = Qnil;
	int i, j, len, outlen;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	if (RB_FLOAT_TYPE_P(sample) || rb_obj_is_kind_of(sample, rb_cNumeric)) {
		switch (flt->decimator_interpolator) {
			case FIR_FILTER_DECIMATOR_FLAG:
				if (flt->counter)
					(*flt->push_one_sample_func)(flt, sample);
				else
					res = (*flt->filter_one_sample_func)(flt, sample, NULL, 1.0);
				flt->counter = (flt->counter + 1) % flt->max_counter;
				break;
			case FIR_FILTER_INTERPOLATOR_FLAG:
				len = flt->max_counter;
				res = rb_ary_new_capa(len);
				rb_ary_store(res, 0, (*flt->filter_one_sample_func)(flt, sample, NULL, (double)len));
				for (i = 1; i < len; i++)
					rb_ary_store(res, i, (*flt->filter_one_sample_func)(flt, flt->zero, NULL, (double)len));
				break;
			default:
				res = (*flt->filter_one_sample_func)(flt, sample, NULL, 1.0);
		}
	}
	else if (rb_class_of(sample) == rb_cArray) {
		len = rb_array_len(sample);
		switch (flt->decimator_interpolator) {
			case FIR_FILTER_DECIMATOR_FLAG:
				outlen = (int)ceil((double)(len - ((flt->max_counter - flt->counter) % flt->max_counter)) / (double)flt->max_counter);
				res = rb_ary_new_capa(outlen);
				for (i = 0, j = 0; i < len; i++) {
					if (flt->counter)
						(*flt->push_one_sample_func)(flt, rb_ary_entry(sample, i));
					else
						rb_ary_store(res, j++, (*flt->filter_one_sample_func)(flt, rb_ary_entry(sample, i), NULL, 1.0));
					flt->counter = (flt->counter + 1) % flt->max_counter;
				}
				break;
			case FIR_FILTER_INTERPOLATOR_FLAG:
				res = rb_ary_new_capa(len*flt->max_counter);
				for (i = 0; i < len; i++) {
					rb_ary_store(res, i*flt->max_counter, (*flt->filter_one_sample_func)(flt, rb_ary_entry(sample, i), NULL, (double)flt->max_counter));
					for (j = 1; j < flt->max_counter; j++)
						rb_ary_store(res, i*flt->max_counter+j, (*flt->filter_one_sample_func)(flt, flt->zero, NULL, (double)flt->max_counter));
				}
				break;
			default:
				res = rb_ary_new_capa(len);
				for (i = 0; i < len; i++)
					rb_ary_store(res, i, (*flt->filter_one_sample_func)(flt, rb_ary_entry(sample, i), NULL, 1.0));
		}
	}
	else
		rb_raise(rb_eArgError, "Expecting a single Numeric or an Array of Numeric");

	return res;
}


/* @param sample [Float, Array<Float>] a single sample or an array of samples to process
 * @return [Array<Float>, Array<Array{Float}>] an array containing the filtered signal, an the input signal delayed by n/2
 */
static VALUE fir_filter_filter_with_delay(VALUE self, VALUE sample)
{
	struct fir_filter *flt;
	VALUE res = Qnil;
	VALUE artwo;
	VALUE delayed = Qnil;
	VALUE rbval;
	int i, j, len, outlen;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	artwo = rb_ary_new_capa(2);

	if (RB_FLOAT_TYPE_P(sample) || rb_obj_is_kind_of(sample, rb_cNumeric)) {
		switch (flt->decimator_interpolator) {
			case FIR_FILTER_DECIMATOR_FLAG:
				if (flt->counter)
					(*flt->push_one_sample_func)(flt, sample);
				else
					res = (*flt->filter_one_sample_func)(flt, sample, &delayed, 1.0);
				flt->counter = (flt->counter + 1) % flt->max_counter;
				break;
			case FIR_FILTER_INTERPOLATOR_FLAG:
				len = flt->max_counter;
				res = rb_ary_new_capa(len);
				delayed = rb_ary_new_capa(len);
				rb_ary_store(res, 0, (*flt->filter_one_sample_func)(flt, sample, &rbval, (double)len));
				rb_ary_store(delayed, 0, rbval);
				for (i = 1; i < len; i++) {
					rb_ary_store(res, i, (*flt->filter_one_sample_func)(flt, flt->zero, &rbval, (double)len));
					rb_ary_store(delayed, i, rbval);
				}
				break;
			default:
				res = (*flt->filter_one_sample_func)(flt, sample, &delayed, 1.0);
		}
	}
	else if (rb_class_of(sample) == rb_cArray) {
		len = rb_array_len(sample);
		switch (flt->decimator_interpolator) {
			case FIR_FILTER_DECIMATOR_FLAG:
				outlen = (int)ceil((double)(len - ((flt->max_counter - flt->counter) % flt->max_counter)) / (double)flt->max_counter);
				res = rb_ary_new_capa(outlen);
				delayed = rb_ary_new_capa(outlen);
				for (i = 0, j = 0; i < len; i++) {
					if (flt->counter)
						(*flt->push_one_sample_func)(flt, rb_ary_entry(sample, i));
					else {
						rb_ary_store(res, j, (*flt->filter_one_sample_func)(flt, rb_ary_entry(sample, i), &rbval, 1.0));
						rb_ary_store(delayed, j++, rbval);
					}
					flt->counter = (flt->counter + 1) % flt->max_counter;
				}
				break;
			case FIR_FILTER_INTERPOLATOR_FLAG:
				res = rb_ary_new_capa(len*flt->max_counter);
				delayed = rb_ary_new_capa(len*flt->max_counter);
				for (i = 0; i < len; i++) {
					rb_ary_store(res, i*flt->max_counter, (*flt->filter_one_sample_func)(flt, rb_ary_entry(sample, i), &rbval, (double)flt->max_counter));
					rb_ary_store(delayed, i*flt->max_counter, rbval);
					for (j = 1; j < flt->max_counter; j++) {
						rb_ary_store(res, i*flt->max_counter+j, (*flt->filter_one_sample_func)(flt, flt->zero, &rbval, (double)flt->max_counter));
						rb_ary_store(delayed, i*flt->max_counter+j, rbval);
					}
				}
				break;
			default:
				res = rb_ary_new_capa(len);
				delayed = rb_ary_new_capa(len);
				for (i = 0; i < len; i++) {
					rb_ary_store(res, i, (*flt->filter_one_sample_func)(flt, rb_ary_entry(sample, i), &rbval, 1.0));
					rb_ary_store(delayed, i, rbval);
				}
		}
	}
	else
		rb_raise(rb_eArgError, "Expecting a single Numeric or an Array of Numeric");

	rb_ary_store(artwo, 0, res);
	rb_ary_store(artwo, 1, delayed);

	return artwo;
}


/* Reset the filter state.
 * @return self
 */
static VALUE fir_filter_reset(VALUE self)
{
	struct fir_filter *flt;
	int i;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);
	for (i = 0; i < flt->state_length; i++)
		flt->state[i] = 0.0f;
	if (flt->stateim) {
		for (i = 0; i < flt->state_length; i++)
			flt->stateim[i] = 0.0f;
	}
	flt->index = 0;
	flt->counter = 0;
	flt->push_one_sample_func = &push_one_sample_float;
	flt->filter_one_sample_func = &filter_one_sample_float;

	return self;
}


static VALUE fir_filter_alloc(VALUE klass)
{
	VALUE obj;
	struct fir_filter *flt;

	flt = ZALLOC(struct fir_filter);

	obj = TypedData_Wrap_Struct(klass, &fir_filter_type, flt);

	flt->b = NULL;
	flt->state = NULL;
	flt->stateim = NULL;
	flt->nb_coefficients = 0;
	flt->state_length = 0;
	flt->index_mask = 0;
	flt->index = 0;
	flt->counter = 0;
	flt->max_counter = 1;
	flt->decimator_interpolator = 0;
	flt->push_one_sample_func = &push_one_sample_float;
	flt->filter_one_sample_func = &filter_one_sample_float;
	flt->zero = DBL2NUM(0.0);

	return obj;
}


/* @overload initialize(n)
 *  @param n [Integer] the filter order
 * @overload initialize(b)
 *  @param b [Array<Float>] the array of coefficients
 */
static VALUE fir_filter_initialize(VALUE self, VALUE b_or_n)
{
	struct fir_filter *flt;
	int i, len;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	if (rb_class_of(b_or_n) == rb_cInteger) {
		flt->nb_coefficients = NUM2INT(b_or_n) + 1;
		if (flt->nb_coefficients < 2)
			rb_raise(rb_eArgError, "Order cannot be less than 1");
		if (flt->nb_coefficients > 1048577)
			rb_raise(rb_eArgError, "Order cannot be more than 1048576");
		flt->b = calloc(flt->nb_coefficients, sizeof(double));
		if (!flt->b)
			rb_raise(rb_eRuntimeError, "Failed to allocate %lu bytes of coefficient storage", flt->nb_coefficients*sizeof(double));
	}
	else if (rb_class_of(b_or_n) == rb_cArray) {
		len = rb_array_len(b_or_n);
		if (len > 1048577)
			rb_raise(rb_eArgError, "Cannot be more than 1048577 coefficients");
		if (len < 1)
			rb_raise(rb_eArgError, "Coefficient array is empty");
		flt->nb_coefficients = len;
		flt->b = calloc(flt->nb_coefficients, sizeof(double));
		if (!flt->b)
			rb_raise(rb_eRuntimeError, "Failed to allocate %lu bytes of coefficient storage", flt->nb_coefficients*sizeof(double));
		for (i = 0; i < len; i++)
			flt->b[flt->nb_coefficients - 1 - i] = (double)(NUM2DBL(rb_ary_entry(b_or_n, i)));
	}
	else
		rb_raise(rb_eArgError, "Expecting the filter order (Integer) or the filter coefficients (Array<Float>)");

	flt->state_length = (int)(pow(2.0, ceil(log2((double)flt->nb_coefficients))));
	flt->index_mask = flt->state_length - 1;
	flt->index = 0;
	flt->state = calloc(flt->state_length, sizeof(double));
	if (!flt->state)
		rb_raise(rb_eRuntimeError, "Failed to allocate %lu bytes of state storage", flt->state_length*sizeof(double));
	flt->stateim = NULL;
	flt->push_one_sample_func = &push_one_sample_float;
	flt->filter_one_sample_func = &filter_one_sample_float;

	return self;
}


/* Push a sample in the filter state.
 * Similar to +filter+, but does not produce an output sample.
 * Used for sample per sample decimators.
 *
 * @param sample [Float, Array<Float>] a single sample or an array of samples to push
 * @return self
 */
static VALUE fir_filter_push(VALUE self, VALUE sample)
{
	struct fir_filter *flt;
	int i, len;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	if (RB_FLOAT_TYPE_P(sample) || rb_obj_is_kind_of(sample, rb_cNumeric)) {
		(*flt->push_one_sample_func)(flt, sample);
	}
	else if (rb_class_of(sample) == rb_cArray) {
		len = rb_array_len(sample);
		for (i = 0; i < len; i++)
			(*flt->push_one_sample_func)(flt, rb_ary_entry(sample, i));
	}
	else
		rb_raise(rb_eArgError, "Expecting a single Numeric or an Array of Numeric");

	return self;
}


/* @return [Boolean] whether the filter is a decimator or not
 */
static VALUE fir_filter_is_decimator(VALUE self)
{
	struct fir_filter *flt;
	VALUE res = Qfalse;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	if (FIR_FILTER_IS_DECIMATOR(flt))
		res = Qtrue;

	return res;
}


/* @return [Boolean] whether the filter is an interpolator or not
 */
static VALUE fir_filter_is_interpolator(VALUE self)
{
	struct fir_filter *flt;
	VALUE res = Qfalse;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	if (FIR_FILTER_IS_INTERPOLATOR(flt))
		res = Qtrue;

	return res;
}


/* @return [Boolean] whether the filter is regular or not (not a decimator or interpolator)
 */
static VALUE fir_filter_is_regular(VALUE self)
{
	struct fir_filter *flt;
	VALUE res = Qfalse;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	if (FIR_FILTER_IS_REGULAR(flt))
		res = Qtrue;

	return res;
}


/* @return [Ingeger, nil] if the filter is a decimator, returns the decimation factor, +nil+ otherwise
 */
static VALUE fir_filter_get_decimation_factor(VALUE self)
{
	struct fir_filter *flt;
	VALUE res = Qnil;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	if (FIR_FILTER_IS_DECIMATOR(flt))
		res = INT2NUM(flt->max_counter);

	return res;
}


/* Make the filter a decimator
 * @param factor [Ingeger] the decimation factor, must be > 1
 */
static VALUE fir_filter_set_decimation_factor(VALUE self, VALUE factor)
{
	struct fir_filter *flt;
	int fctr;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	fctr = NUM2INT(factor);
	if (fctr > 1) {
		flt->decimator_interpolator = FIR_FILTER_DECIMATOR_FLAG;
		flt->max_counter = fctr;
	}
	else {
		flt->decimator_interpolator = 0;
		flt->max_counter = 1;
	}
	flt->counter = 0;

	return self;
}


/* @return [Ingeger, nil] if the filter is an interpolator, returns the interpolation factor, +nil+ otherwise
 */
static VALUE fir_filter_get_interpolation_factor(VALUE self)
{
	struct fir_filter *flt;
	VALUE res = Qnil;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	if (FIR_FILTER_IS_INTERPOLATOR(flt))
		res = INT2NUM(flt->max_counter);

	return res;
}


/* Make the filter an interpolator
 * @param factor [Ingeger] the interpolation factor, must be > 1
 */
static VALUE fir_filter_set_interpolation_factor(VALUE self, VALUE factor)
{
	struct fir_filter *flt;
	int fctr;

	TypedData_Get_Struct(self, struct fir_filter, &fir_filter_type, flt);

	fctr = NUM2INT(factor);
	if (fctr > 1) {
		flt->decimator_interpolator = FIR_FILTER_INTERPOLATOR_FLAG;
		flt->max_counter = fctr;
	}
	else {
		flt->decimator_interpolator = 0;
		flt->max_counter = 1;
	}
	flt->counter = 0;

	return self;
}


void Init_fir_filter()
{
	id_real = rb_intern("real");
	id_imag = rb_intern("imag");

	c_FIR = rb_define_class_under(m_Roctave, "FirFilter", rb_cData);

	rb_define_alloc_func(c_FIR, fir_filter_alloc);
	rb_define_method(c_FIR, "initialize",         fir_filter_initialize,                  1);
	rb_define_method(c_FIR, "order",              fir_filter_order,                       0);
	rb_define_method(c_FIR, "nb_coefficients",    fir_filter_nb_coefficients,             0);
	rb_define_method(c_FIR, "get_num_coefficient_at", fir_filter_get_coefficient_at,      1);
	rb_define_method(c_FIR, "set_num_coefficient_at", fir_filter_set_coefficient_at,      2);
	rb_define_method(c_FIR, "get_den_coefficient_at", fir_filter_get_denominator_at,      1);
	rb_define_method(c_FIR, "set_den_coefficient_at", fir_filter_set_denominator_at,      2);
	rb_define_alias(c_FIR, "get_coefficient_at", "get_num_coefficient_at");
	rb_define_alias(c_FIR, "set_coefficient_at", "set_num_coefficient_at");
	rb_define_method(c_FIR, "reset!",             fir_filter_reset,                       0);
	rb_define_method(c_FIR, "numerator",          fir_filter_coefficients,                0);
	rb_define_method(c_FIR, "denominator",        fir_filter_denominator,                 0);
	rb_define_alias(c_FIR, "coefficients", "numerator");                                 
	rb_define_method(c_FIR, "filter",             fir_filter_filter,                      1);
	rb_define_method(c_FIR, "filter_with_delay",  fir_filter_filter_with_delay,           1);
	rb_define_method(c_FIR, "push",               fir_filter_push,                        1);
	rb_define_alias(c_FIR, "<<", "push");
	rb_define_method(c_FIR, "regular?",           fir_filter_is_regular,                  0);
	rb_define_method(c_FIR, "decimator?",         fir_filter_is_decimator,                0);
	rb_define_method(c_FIR, "interpolator?",      fir_filter_is_interpolator,             0);
	rb_define_method(c_FIR, "decimation_factor",  fir_filter_get_decimation_factor,       0);
	rb_define_method(c_FIR, "decimation_factor=", fir_filter_set_decimation_factor,       1);
	rb_define_method(c_FIR, "interpolation_factor",  fir_filter_get_interpolation_factor, 0);
	rb_define_method(c_FIR, "interpolation_factor=", fir_filter_set_interpolation_factor, 1);
}



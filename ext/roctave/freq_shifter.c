/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of Roctave
 * 
 * Roctave is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Roctave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Roctave. If not, see <https://www.gnu.org/licenses/>.
 */


#include <ruby.h>
#include "roctave.h"
#include "freq_shifter.h"

VALUE c_FreqShifter;


static ID id_real;
static ID id_imag;
static ID id_freq;
static ID id_fs;
static ID id_initial_phase;


struct freq_shifter {
	double phase;
	double sampling_frequency;
	double frequency;
	double phase_increment;
};


static void freq_shifter_free(void *p)
{
	struct freq_shifter *frqsh = (struct freq_shifter*)p;

	xfree(frqsh);
}


static size_t freq_shifter_size(const void* data)
{
	return sizeof(struct freq_shifter);
}


static const rb_data_type_t freq_shifter_type = {
	.wrap_struct_name = "roctave_freq_shifter_struct",
	.function = {
		.dmark = NULL,
		.dfree = freq_shifter_free,
		.dsize = freq_shifter_size,
	},
	.data = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE freq_shifter_alloc(VALUE klass)
{
	VALUE obj;
	struct freq_shifter *frqsh;

	frqsh = ZALLOC(struct freq_shifter);

	obj = TypedData_Wrap_Struct(klass, &freq_shifter_type, frqsh);

	frqsh->phase = 0.0;
	frqsh->sampling_frequency = 1.0;
	frqsh->frequency = 1.0;
	frqsh->phase_increment = 2.0*M_PI*frqsh->frequency/frqsh->sampling_frequency;

	return obj;
}


/* @overload initialize(freq: 0.5, fs: 1.0, initial_phase: 0.0)
 * @param freq [Float] the amount of frequency to shift
 * @param fs [Float] the sampling frequency
 * @param initial_phase [Float] the initial phase of the oscillator
 * @return [Roctave::FreqShifter]
 */
static VALUE freq_shifter_initialize(int argc, VALUE *argv, VALUE self)
{
	struct freq_shifter *frqsh;
	VALUE hash;
	const ID kwkeys[] = {id_freq, id_fs, id_initial_phase};
	VALUE kwvalues[] = {Qundef, Qundef, Qundef};

	// int i;
	// for (i = 0; i < argc; i++)
	// 	rb_warn("argv[%d] = %"PRIsVALUE, i, rb_class_name(rb_obj_class(argv[i])));

	rb_scan_args(argc, argv, ":", &hash);

	if (!NIL_P(hash))
		rb_get_kwargs(hash, kwkeys, 0, 3, kwvalues);

	TypedData_Get_Struct(self, struct freq_shifter, &freq_shifter_type, frqsh);

	if (kwvalues[0] != Qundef)
		frqsh->frequency = NUM2DBL(kwvalues[0]);
	else
		frqsh->frequency = 0.5;

	if (kwvalues[1] != Qundef)
		if (NUM2DBL(kwvalues[1]) > 0.0) frqsh->sampling_frequency = NUM2DBL(kwvalues[1]);
	else
		frqsh->sampling_frequency = 1.0;

	if (kwvalues[2] != Qundef)
		frqsh->phase = fmod(NUM2DBL(kwvalues[2]), 2.0*M_PI);
	else
		frqsh->phase = 0.0;

	frqsh->phase_increment = 2.0*M_PI*frqsh->frequency/frqsh->sampling_frequency;

	// rb_warn(" ");
	// rb_warn("phase = %g", frqsh->phase);
	// rb_warn("frequency = %g", frqsh->frequency);
	// rb_warn("sampling_frequency = %g", frqsh->sampling_frequency);
	// rb_warn(" ");

	return self;
}


/* Get the current phase of the oscillator.
 * @return [Float] the current phase of the oscillator
 */
static VALUE freq_shifter_get_phase(VALUE self)
{
	struct freq_shifter *frqsh;

	TypedData_Get_Struct(self, struct freq_shifter, &freq_shifter_type, frqsh);

	return DBL2NUM(frqsh->phase);
}


/* Set the current phase of the oscillator.
 * @param phase [Float] the requested phase of the oscillator
 * @return [Float] the phase of the oscillator
 */
static VALUE freq_shifter_set_phase(VALUE self, VALUE phase)
{
	struct freq_shifter *frqsh;

	TypedData_Get_Struct(self, struct freq_shifter, &freq_shifter_type, frqsh);

	frqsh->phase = fmod(NUM2DBL(phase), 2.0*M_PI);

	return DBL2NUM(frqsh->phase);
}


/* Get the sampling frequency
 * @return [Float] the sampling frequency
 */
static VALUE freq_shifter_get_sampling_frequency(VALUE self)
{
	struct freq_shifter *frqsh;

	TypedData_Get_Struct(self, struct freq_shifter, &freq_shifter_type, frqsh);

	return DBL2NUM(frqsh->sampling_frequency);
}


/* Set the sampling frequency
 * @param fs [Float] the requested sampling frequency. Must be > 0.
 * @return [Float] the sampling frequency
 */
static VALUE freq_shifter_set_sampling_frequency(VALUE self, VALUE fs)
{
	struct freq_shifter *frqsh;

	TypedData_Get_Struct(self, struct freq_shifter, &freq_shifter_type, frqsh);

	if (NUM2DBL(fs) > 0.0)
		frqsh->sampling_frequency = NUM2DBL(fs);

	frqsh->phase_increment = 2.0*M_PI*frqsh->frequency/frqsh->sampling_frequency;

	return DBL2NUM(frqsh->sampling_frequency);
}


/* Get the frequency
 * @return [Float] the frequency
 */
static VALUE freq_shifter_get_frequency(VALUE self)
{
	struct freq_shifter *frqsh;

	TypedData_Get_Struct(self, struct freq_shifter, &freq_shifter_type, frqsh);

	return DBL2NUM(frqsh->frequency);
}


/* Set the frequency
 * @param f [Float] the requested frequency. Must be > 0.
 * @return [Float] the frequency
 */
static VALUE freq_shifter_set_frequency(VALUE self, VALUE f)
{
	struct freq_shifter *frqsh;

	TypedData_Get_Struct(self, struct freq_shifter, &freq_shifter_type, frqsh);

	frqsh->sampling_frequency = NUM2DBL(f);

	frqsh->phase_increment = 2.0*M_PI*frqsh->frequency/frqsh->sampling_frequency;

	return DBL2NUM(frqsh->frequency);
}


static VALUE freq_shifter_shift_one_sample(struct freq_shifter *frqsh, VALUE sample)
{
	const double i = cos(frqsh->phase);
	const double q = sin(frqsh->phase);
	double real, imag;
	VALUE res;

	if (rb_class_of(sample) == rb_cComplex) {
		real = NUM2DBL(rb_funcallv(sample, id_real, 0, NULL));
		imag = NUM2DBL(rb_funcallv(sample, id_imag, 0, NULL));
		res = rb_complex_raw(
				DBL2NUM(real*i - imag*q), 
				DBL2NUM(real*q + imag*i));
	}
	else {
		real = NUM2DBL(sample);
		res = rb_complex_raw(
				DBL2NUM(real*i), 
				DBL2NUM(real*q));
	}

	frqsh->phase = fmod(frqsh->phase + frqsh->phase_increment, 2*M_PI);

	return res;
}


static VALUE freq_shifter_am_one_sample(struct freq_shifter *frqsh, VALUE sample)
{
	const double i = cos(frqsh->phase);
	double real, imag;
	VALUE res;

	if (rb_class_of(sample) == rb_cComplex) {
		real = NUM2DBL(rb_funcallv(sample, id_real, 0, NULL));
		imag = NUM2DBL(rb_funcallv(sample, id_imag, 0, NULL));
		res = rb_complex_raw(
				DBL2NUM(real*i), 
				DBL2NUM(imag*i));
	}
	else
		res = DBL2NUM(NUM2DBL(sample) * i);

	frqsh->phase = fmod(frqsh->phase + frqsh->phase_increment, 2*M_PI);

	return res;
}


/* Multiply the signal with exp(2*i*pi*f*t)
 * @param signal [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
 * @return [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
 */
static VALUE freq_shifter_shift(VALUE self, VALUE sig)
{
	struct freq_shifter *frqsh;
	VALUE res;
	int len, i;

	TypedData_Get_Struct(self, struct freq_shifter, &freq_shifter_type, frqsh);

	if (rb_class_of(sig) == rb_cArray) {
		len = rb_array_len(sig);
		res = rb_ary_new_capa(len);
		for (i = 0; i < len; i++)
			rb_ary_store(res, i, freq_shifter_shift_one_sample(frqsh, rb_ary_entry(sig, i)));
	}
	else {
		res = freq_shifter_shift_one_sample(frqsh, sig);
	}

	return res;
}


/* Multiply the signal with cos(2*i*pi*f*t)
 * @param signal [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
 * @return [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
 */
static VALUE freq_shifter_am(VALUE self, VALUE sig)
{
	struct freq_shifter *frqsh;
	VALUE res;
	int len, i;

	TypedData_Get_Struct(self, struct freq_shifter, &freq_shifter_type, frqsh);

	if (rb_class_of(sig) == rb_cArray) {
		len = rb_array_len(sig);
		res = rb_ary_new_capa(len);
		for (i = 0; i < len; i++)
			rb_ary_store(res, i, freq_shifter_am_one_sample(frqsh, rb_ary_entry(sig, i)));
	}
	else {
		res = freq_shifter_am_one_sample(frqsh, sig);
	}

	return res;
}


#if 0
// Makes segmentation faults, maybe because rbo is garbage collected
// befor it can finish its shift.

/* Multiply the signal with exp(2*i*pi*f*t)
 * @overload shift(signal, freq: 0.5, fs: 1.0, initial_phase: 0.0)
 * @param signal [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
 * @param freq [Float] the amount of frequency to shift
 * @param fs [Float] the sampling frequency
 * @param initial_phase [Float] the initial phase of the oscillator
 * @return [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
 */
static VALUE freq_shifter_module_shift(int argc, VALUE *argv, VALUE self)
{
	VALUE sig;
	VALUE hash;
	VALUE rbo;
	VALUE res;

	rb_scan_args(argc, argv, "1:", &sig, &hash);

	if (NIL_P(hash))
		hash = rb_hash_new();

	rbo = rb_class_new_instance(1, &hash, c_FreqShifter);
	res = freq_shifter_shift(rbo, sig);

	return res;
}


/* Multiply the signal with cos(2*i*pi*f*t)
 * @overload shift(signal, freq: 0.5, fs: 1.0, initial_phase: 0.0)
 * @param signal [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
 * @param freq [Float] the amount of frequency to shift
 * @param fs [Float] the sampling frequency
 * @param initial_phase [Float] the initial phase of the oscillator
 * @return [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
 */
static VALUE freq_shifter_module_am(int argc, VALUE *argv, VALUE self)
{
	VALUE sig;
	VALUE hash;
	VALUE rbo;

	rb_scan_args(argc, argv, "1:", &sig, &hash);

	if (NIL_P(hash))
		hash = rb_hash_new();

	rbo = rb_class_new_instance(1, &hash, c_FreqShifter);

	return freq_shifter_am(rbo, sig);
}
#endif


void Init_freq_shifter()
{
	id_real = rb_intern("real");
	id_imag = rb_intern("imag");
	id_freq = rb_intern("freq");
	id_fs   = rb_intern("fs");
	id_initial_phase = rb_intern("initial_phase");

	c_FreqShifter = rb_define_class_under(m_Roctave, "FreqShifter", rb_cData);

	rb_define_alloc_func(c_FreqShifter, freq_shifter_alloc);
	//rb_define_singleton_method(c_FreqShifter, "shift",              freq_shifter_module_shift, -1);
	//rb_define_singleton_method(c_FreqShifter, "amplitude_modulate", freq_shifter_module_am,    -1);
	rb_define_method(c_FreqShifter, "initialize",          freq_shifter_initialize,            -1);
	rb_define_method(c_FreqShifter, "shift",               freq_shifter_shift,                  1);
	rb_define_method(c_FreqShifter, "amplitude_modulate",  freq_shifter_am,                     1);
	rb_define_method(c_FreqShifter, "frequency",           freq_shifter_get_frequency,          0);
	rb_define_method(c_FreqShifter, "frequency=",          freq_shifter_set_frequency,          1);
	rb_define_method(c_FreqShifter, "sampling_frequency",  freq_shifter_get_sampling_frequency, 0);
	rb_define_method(c_FreqShifter, "sampling_frequency=", freq_shifter_set_sampling_frequency, 1);
	rb_define_method(c_FreqShifter, "phase",               freq_shifter_get_phase,              0);
	rb_define_method(c_FreqShifter, "phase=",              freq_shifter_set_phase,              1);
}


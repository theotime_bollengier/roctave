/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of Roctave
 * 
 * Roctave is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Roctave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Roctave. If not, see <https://www.gnu.org/licenses/>.
 */


#include <errno.h>
#include <ruby.h>
#include "roctave.h"
#include "cu8_file_reader.h"


VALUE c_CU8FileReader;


struct cu8_file_reader {
	FILE         *file;
};


static void cu8_file_reader_free(void *p)
{
	struct cu8_file_reader *fr = (struct cu8_file_reader*)p;

	if (fr->file)
		fclose(fr->file);

	xfree(fr);
}


static size_t cu8_file_reader_size(const void* data)
{
	(void)data;

	return sizeof(struct cu8_file_reader);
}


static const rb_data_type_t cu8_file_reader_type = {
	.wrap_struct_name = "roctave_cu8_file_reader_struct",
	.function = {
		.dmark = NULL,
		.dfree = cu8_file_reader_free,
		.dsize = cu8_file_reader_size,
	},
	.data = NULL,
	.flags = RUBY_TYPED_FREE_IMMEDIATELY,
};


static VALUE cu8_file_reader_alloc(VALUE klass)
{
	VALUE obj;
	struct cu8_file_reader *fr;

	fr = ZALLOC(struct cu8_file_reader);

	obj = TypedData_Wrap_Struct(klass, &cu8_file_reader_type, fr);

	fr->file = NULL;

	return obj;
}


/* Close the opened file
 */
static VALUE cu8_file_reader_close(VALUE self)
{
	struct cu8_file_reader *fr;

	TypedData_Get_Struct(self, struct cu8_file_reader, &cu8_file_reader_type, fr);

	if (fr->file != NULL) {
		fclose(fr->file);
		fr->file = NULL;
	}

	return self;
}


/* @return [Boolean] whether the CU8FileReader has an opened file or not
 */
static VALUE cu8_file_reader_closed(VALUE self)
{
	struct cu8_file_reader *fr;
	VALUE res = Qtrue;

	TypedData_Get_Struct(self, struct cu8_file_reader, &cu8_file_reader_type, fr);

	if (fr->file != NULL)
		res = Qfalse;

	return res;
}


/* @param filename [String] the path of the file to open
 */
static VALUE cu8_file_reader_open(VALUE self, VALUE filename)
{
	struct cu8_file_reader *fr;
	VALUE expanded_filename;
	const char *expfname;

	TypedData_Get_Struct(self, struct cu8_file_reader, &cu8_file_reader_type, fr);

	cu8_file_reader_close(self);

	if (rb_class_of(filename) != rb_cString)
		rb_raise(rb_eArgError, "Expecting the file path as a string");

	expanded_filename = rb_funcallv(rb_cFile, rb_intern("expand_path"), 1, &filename);
	expfname = StringValueCStr(expanded_filename);
	if ((fr->file = fopen(expfname, "rb")) == NULL)
		rb_raise(rb_eRuntimeError, "Cannot open file \"%s\": %s", expfname, strerror(errno));

	return self;
}


/* @param filename [String] the path of the file to read
 */
static VALUE cu8_file_reader_initialize(VALUE self, VALUE filename)
{
	return cu8_file_reader_open(self, filename);
}


static inline void cu8_file_reader_get_size_and_position(struct cu8_file_reader *fr, long *position, long *size)
{
	long pos;
	long sz;

	pos = ftell(fr->file);
	if (position)
		*position = pos/2;
	if (size) {
		fseek(fr->file, 0, SEEK_END);
		sz = ftell(fr->file);
		fseek(fr->file, pos, SEEK_SET);
		*size = sz/2;
	}
}


/* @return [Boolean, nil] if the end of the file has been reached, +nil+ if it is closed
 */
static VALUE cu8_file_reader_eof(VALUE self)
{
	struct cu8_file_reader *fr;
	VALUE res = Qfalse;
	long pos, size;

	TypedData_Get_Struct(self, struct cu8_file_reader, &cu8_file_reader_type, fr);

	if (fr->file == NULL)
		return Qnil;

	cu8_file_reader_get_size_and_position(fr, &pos, &size);

	if (pos >= size)
		res = Qtrue;

	return res;
}


static VALUE cu8_file_reader_rewind(VALUE self)
{
	struct cu8_file_reader *fr;

	TypedData_Get_Struct(self, struct cu8_file_reader, &cu8_file_reader_type, fr);

	if (fr->file == NULL)
		return Qnil;

	rewind(fr->file);

	return self;
}


/* @return [Integer] the number of complex numbers in the file
 */
static VALUE cu8_file_reader_length(VALUE self)
{
	struct cu8_file_reader *fr;
	long size;

	TypedData_Get_Struct(self, struct cu8_file_reader, &cu8_file_reader_type, fr);

	if (fr->file == NULL)
		return Qnil;

	cu8_file_reader_get_size_and_position(fr, NULL, &size);

	return LONG2NUM(size);
}


/* @return [Integer] the file position indicator in terms of complex numbers
 */
static VALUE cu8_file_reader_position(VALUE self)
{
	struct cu8_file_reader *fr;
	long pos;

	TypedData_Get_Struct(self, struct cu8_file_reader, &cu8_file_reader_type, fr);

	if (fr->file == NULL)
		return Qnil;

	cu8_file_reader_get_size_and_position(fr, &pos, NULL);

	return LONG2NUM(pos);
}


/* @return [Float] the file position indicator divided by the file length
 */
static VALUE cu8_file_reader_normalized_position(VALUE self)
{
	struct cu8_file_reader *fr;
	long pos, size;

	TypedData_Get_Struct(self, struct cu8_file_reader, &cu8_file_reader_type, fr);

	if (fr->file == NULL)
		return Qnil;

	cu8_file_reader_get_size_and_position(fr, &pos, &size);

	return DBL2NUM((double)pos / (double)size);
}


/* @overload read()
 *  Returns an array of complex numbers read from the file content
 *  @return [Array<Float>] array of complex numbers read from the file
 * @overload read(len)
 *  Reads at most +len+ complex numbers from the file.
 *  @return [Array<Float>] array of at most +len+ complex numbers read from the file
 */
static VALUE cu8_file_reader_read(int argc, VALUE *argv, VALUE self)
{
	struct cu8_file_reader *fr;
	VALUE rblen;
	long pos, size;
	int len, r, to_read, readden, i;
	unsigned char *bufc;
	VALUE res;

	rb_scan_args(argc, argv, "01", &rblen);

	TypedData_Get_Struct(self, struct cu8_file_reader, &cu8_file_reader_type, fr);

	if (fr->file == NULL)
		return Qnil;

	cu8_file_reader_get_size_and_position(fr, &pos, &size);
	len = (int)(size - pos);

	if (rblen != Qnil) {
		if (NUM2INT(rblen) < 0)
			rb_raise(rb_eArgError, "Requested length cannot be less than 0");
		if (len > NUM2INT(rblen))
			len = NUM2INT(rblen);
	}

	if (len <= 0)
		return rb_ary_new_capa(0);

	bufc = malloc(len*2*sizeof(unsigned char));
	if (bufc == NULL)
		rb_raise(rb_eRuntimeError, "Failed to allocate %d bytes for internal buffer", len);

	to_read = len*2;
	readden = 0;
	do {
		r = fread(bufc + readden, sizeof(unsigned char), to_read, fr->file);
		if (r < 0)
			rb_raise(rb_eRuntimeError, "Error reading file: %s", strerror(errno));
		readden += r;
		to_read -= r;
	} while (to_read > 0 && r > 0);
	len = readden / 2;

	res = rb_ary_new_capa(len);
	for (i = 0; i < len; i++)
		rb_ary_store(res, i, rb_complex_raw(DBL2NUM(((double)bufc[2*i+0] - 128.0)/128.0), DBL2NUM(((double)bufc[2*i+1] - 128.0)/128.0)));

	free(bufc);

	return res;
}


void Init_cu8_file_reader()
{
	c_CU8FileReader = rb_define_class_under(m_Roctave, "CU8FileReader", rb_cData);

	rb_define_alloc_func(c_CU8FileReader, cu8_file_reader_alloc);
	rb_define_method(c_CU8FileReader, "initialize",          cu8_file_reader_initialize,          1);
	rb_define_method(c_CU8FileReader, "open",                cu8_file_reader_open,                1);
	rb_define_method(c_CU8FileReader, "close",               cu8_file_reader_close,               0);
	rb_define_method(c_CU8FileReader, "read",                cu8_file_reader_read,               -1);
	rb_define_method(c_CU8FileReader, "eof?",                cu8_file_reader_eof,                 0);
	rb_define_method(c_CU8FileReader, "rewind",              cu8_file_reader_rewind,              0);
	rb_define_method(c_CU8FileReader, "length",              cu8_file_reader_length,              0);
	rb_define_method(c_CU8FileReader, "position",            cu8_file_reader_position,            0);
	rb_define_method(c_CU8FileReader, "normalized_position", cu8_file_reader_normalized_position, 0);
	rb_define_method(c_CU8FileReader, "closed?",             cu8_file_reader_closed,              0);
}



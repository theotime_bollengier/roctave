/* Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
 *
 * This file is part of Roctave
 * 
 * Roctave is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Roctave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Roctave. If not, see <https://www.gnu.org/licenses/>.
 */


#include <ruby.h>
#include "roctave.h"
#include "fir_filter.h"
#include "iir_filter.h"
#include "cu8_file_reader.h"
#include "freq_shifter.h"

VALUE m_Roctave;

void Init_roctave()
{
	m_Roctave = rb_const_get_at(rb_cObject, rb_intern("Roctave"));

	Init_fir_filter();
	Init_iir_filter();
	Init_cu8_file_reader();
	Init_freq_shifter();
}


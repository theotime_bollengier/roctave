require File.expand_path('../lib/roctave/version', __FILE__)

Gem::Specification.new do |s|
	s.name = "roctave"
	s.version = Roctave::VERSION
	s.date = Time.now.strftime '%Y-%m-%d'
	s.summary = "Some Matlab / Gnu Octave functions implemented in Ruby."
	s.description = "Roctave is an attempt to provide some Matlab / Gnu Octave functions in a Ruby gem to generate and analyze digital filters."
	s.license = 'GPL-3.0+'
	s.authors = ["Théotime Bollengier"]
	s.email = 'theotime.bollengier@gmail.com'
	s.homepage = 'https://gitlab.com/theotime_bollengier/roctave'
	s.add_runtime_dependency 'gnuplot', '~> 2.6', '>= 2.6.2'
	s.files = [
		'LICENSE',
		'README.md',
		'roctave.gemspec',
		'ext/roctave/extconf.rb',
		'ext/roctave/roctave.c',
		'ext/roctave/roctave.h',
		'ext/roctave/fir_filter.c',
		'ext/roctave/fir_filter.h',
		'ext/roctave/iir_filter.c',
		'ext/roctave/iir_filter.h',
		'ext/roctave/cu8_file_reader.c',
		'ext/roctave/cu8_file_reader.h',
		'ext/roctave/freq_shifter.c',
		'ext/roctave/freq_shifter.h',
		'lib/roctave.rb',
		'lib/roctave/version.rb',
		'lib/roctave/bilinear.rb',
		'lib/roctave/butter.rb',
		'lib/roctave/cheby.rb',
		'lib/roctave/dft.rb',
		'lib/roctave/finite_difference_coefficients.rb',
		'lib/roctave/fir1.rb',
		'lib/roctave/fir2.rb',
		'lib/roctave/fir_design.rb',
		'lib/roctave/firls.rb',
		'lib/roctave/firpm.rb',
		'lib/roctave/filter.rb',
		'lib/roctave/fir.rb',
		'lib/roctave/iir.rb',
		'lib/roctave/cu8_file_reader.rb',
		'lib/roctave/freq_shifter.rb',
		'lib/roctave/freqz.rb',
		'lib/roctave/interp1.rb',
		'lib/roctave/plot.rb',
		'lib/roctave/poly.rb',
		'lib/roctave/roots.rb',
		'lib/roctave/sftrans.rb',
		'lib/roctave/window.rb',
		'samples/butter.rb',
		'samples/cheby.rb',
		'samples/dft.rb',
		'samples/differentiator_frequency_scaling.rb',
		'samples/differentiator.rb',
		'samples/fft.rb',
		'samples/finite_difference_coefficient.rb',
		'samples/fir1.rb',
		'samples/fir2bank.rb',
		'samples/fir2.rb',
		'samples/fir2_windows.rb',
		'samples/fir_low_pass.rb',
		'samples/firls.rb',
		'samples/firpm.rb',
		'samples/hilbert_transformer_frequency_scaling.rb',
		'samples/hilbert_transformer.rb',
		'samples/plot.rb',
		'samples/stem.rb',
		'samples/type1.rb',
		'samples/type3.rb',
		'samples/windows.rb'
	]
	s.extensions = [
		'ext/roctave/extconf.rb'
	]
end


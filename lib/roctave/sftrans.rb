## Copyright (C) 1999-2001 Paul Kienzle <pkienzle@users.sf.net> (GNU Octave implementation)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave

	##
	# Transform band edges of a generic lowpass filter (cutoff at W=1)
	# represented in splane zero-pole-gain form.  W is the edge of the
	# target filter (or edges if band pass or band stop). Stop is true for
	# high pass and band stop filters or false for low pass and band pass
	# filters. Filter edges are specified in radians, from 0 to pi (the
	# nyquist frequency).
	#
	# References:
	#
	# Proakis & Manolakis (1992). Digital Signal Processing. New York:
	# Macmillan Publishing Company.
	#
	# @param sz [Array<Numeric>] Zeros in S plane
	# @param sp [Array<Numeric>] Poless in S plane
	# @param sg [Numeric] Gain in S plane
	# @param w  [Array<Float>] Cuttoff frequency. It is an array of either one or two Floats
	# @param stop  [Boolean] True for high pass and band stop, false for low pass and band pass
	# @return [Array<Array{Numeric}>] An array of transformed zeros, poles and gain in the S plane [sz, sp, sg]
	def self.sftrans(sz, sp, sg, w, stop)
		case w
		when Numeric
			w = [w.to_f]
		when Range
			w = [w.begin.to_f, w.end.to_f].sort
		when Array
			case w.length
			when 1
				w = [w.first.to_f]
			when 2
				w = [w.first.to_f, w.last.to_f].sort
			else
				raise ArgumentError.new "Must specify at most 2 frequency band edges"
			end
		else
			raise ArgumentError.new "w must be a Float, a Range or an array"
		end
		raise ArgumentError.new "band edges must not be equal" if w.length > 1 and w.first == w.last
		stop = not(not(stop))

		c = 1.0
		p = sp.length
		z = sz.length
		if z > p or p == 0 then
			raise ArgumentError.new "sftrans: must have at least as many poles as zeros in s-plane"
		end

		if w.length == 2 then
			fl = w.first
			fh = w.last
			if stop then
				## ----------------  -------------------------  ------------------------ ##
				## Band Stop         zero: b ± sqrt(b^2-FhFl)   pole: b ± sqrt(b^2-FhFl) ##
				##        S(Fh-Fl)   pole: ±sqrt(-FhFl)         zero: ±sqrt(-FhFl)       ##
				## S -> C --------   gain: -x                   gain: -1/x               ##
				##        S^2+FhFl   b=C/x (Fh-Fl)/2            b=C/x (Fh-Fl)/2          ##
				## ----------------  -------------------------  ------------------------ ##
				if sz.empty? then
					sg = sg * (1.0 / sp.inject(1.0){|m, v| m * (-v)}).real
				elsif sp.empty? then
					sg = sg * (sz.inject(1.0){|m, v| m * (-v)}).real
				else
					sg = sg * (sz.inject(1.0){|m, v| m * (-v)} / sp.inject(1.0){|m, v| m * (-v)}).real
				end
				b = sp.collect{|v| (c*(fh-fl)/2.0)/v}
				sp = b.collect{|v| v + Math.sqrt(v**2 - fh*fl)} + b.collect{|v| v - Math.sqrt(v**2 - fh*fl)}
				ext = [Math.sqrt(Complex(-fh*fl)), -Math.sqrt(Complex(-fh*fl))]
				if sz.empty? then
					sz = (1 .. 2*p).collect{|i| ext[i % 2]}
				else
					b = sz.collect{|v| (c*(fh-fl)/2.0)/v}
					sz = b.collect{|v| v + Math.sqrt(v**2-fh*fl)} + b.collect{|v| v - Math.sqrt(v**2-fh*fl)}
					if p > z then
						sz = sz + (1 .. 2*(p-z)).collect{|i| ext[i%2]}
					end
				end
			else
				## ----------------  -------------------------  ------------------------ ##
				## Band Pass         zero: b ± sqrt(b^2-FhFl)   pole: b ± sqrt(b^2-FhFl) ##
				##        S^2+FhFl   pole: 0                    zero: 0                  ##
				## S -> C --------   gain: C/(Fh-Fl)            gain: (Fh-Fl)/C          ##
				##        S(Fh-Fl)   b=x/C (Fh-Fl)/2            b=x/C (Fh-Fl)/2          ##
				## ----------------  -------------------------  ------------------------ ##
				sg = sg * (c/(fh-fl))**(z-p)
				b = sp.collect{|v| v*((fh-fl)/(2.0*c))}
				sp = b.collect{|v| v + Math.sqrt(v**2-fh*fl)} + b.collect{|v| v - Math.sqrt(v**2-fh*fl)}
				if sz.empty? then
					sz = Array.new(p){0.0}
				else
					b = sz.collect{|v| v*((fh-fl)/(2.0*c))}
					sz = b.collect{|v| v + Math.sqrt(v**2-fh*fl)} + b.collect{|v| v - Math.sqrt(v**2-fh*fl)}
					if p > z then
						sz = sz + Array.new(p-z){0.0}
					end
				end
			end
		else
			fc = w.first
			if stop then
				## ----------------  -------------------------  ------------------------ ## 
				## High Pass         zero: Fc C/x               pole: Fc C/x             ##
				## S -> C Fc/S       pole: 0                    zero: 0                  ##
				##                   gain: -x                   gain: -1/x               ##
				## ----------------  -------------------------  ------------------------ ##
				if sz.empty? then
					sg = sg * (1.0 / sp.inject(1.0){|m, v| m * (-v)}).real
				elsif sp.empty? then
					sg = sg * sz.inject(1.0){|m, v| m * (-v)}.real
				else
					sg = sg * (sz.inject(1.0){|m, v| m * (-v)} / sp.inject(1.0){|m, v| m * (-v)}).real
				end
				sp = sp.collect{|v| c*fc/v}
				if sz.empty? then
					sz = Array.new(p){0.0}
				else
					sz = sz.collect{|v| c*fc/v}
					if p > z then
						sz = sz + Array.new(p-z){0.0}
					end
				end
			else
				## ----------------  -------------------------  ------------------------ ##
				## Low Pass          zero: Fc x/C               pole: Fc x/C             ##
				## S -> C S/Fc       gain: C/Fc                 gain: Fc/C               ##
				## ----------------  -------------------------  ------------------------ ##
				sg = sg * (c/fc)**(z-p)
				sp = sp.collect{|v| fc * v / c}
				sz = sz.collect{|v| fc * v / c}
			end
		end

		return [sz, sp, sg]
	end

end


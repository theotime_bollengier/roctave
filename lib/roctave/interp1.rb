## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	##
	# Linearly interpolate between points.
	# @param x [Array<Float>] X coordinates of given points
	# @param y [Array] Y value of given points at X coordinates
	# @param xi [Array<Float>] X coordinates of the requested interpolated points
	# @return [Array] the Y value of interpolated points at xi coordinates
	def self.interp1 (x, y, xi)
		[x, y, xi].each.with_index do |a, i| 
			raise ArgumentError.new "Argument #{i+1} must be an array" unless a.kind_of?(Array)
		end
		raise ArgumentError.new "Argument 1 and 2 must be arrays of the same length" unless x.length == y.length

		xr = x.reverse
		xi.collect.with_index do |v, i|
			leftmost = nil
			leftmost_index = nil
			x.each.with_index do |e, j|
				if (e < v) or (e == v and leftmost != v) then
					leftmost = e
					leftmost_index = j
				end
			end
			if leftmost.nil? then
				leftmost = x.last
				leftmost_index = x.length - 1
			end

			rightmost = nil
			rightmost_index = nil
			xr.each.with_index do |e, j|
				if (e > v) or (e == v and rightmost != v) then
					rightmost = e
					rightmost_index = x.length - 1 - j
				end
			end
			if rightmost.nil? then
				rightmost = x.first
				rightmost_index = 0
			end

			if leftmost_index > rightmost_index then
				leftmost,rightmost = rightmost,leftmost
				leftmost_index,rightmost_index = rightmost_index,leftmost_index
			end

			#puts "[#{i}]  #{leftmost_index} - #{rightmost_index}"

			ml = y[leftmost_index]
			mr = y[rightmost_index]

			if (rightmost - leftmost) == 0 then
				(ml + mr) / 2.0
			else
				ml*(rightmost - v)/(rightmost - leftmost) + mr*(v - leftmost)/(rightmost - leftmost)
			end
		end
	end
end


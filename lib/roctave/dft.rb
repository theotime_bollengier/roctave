## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	# @!group Fourier transform

	##
	# Compute the discrete Fourier transform of +x+.
	# If called with two arguments, +n+ is expected to be an integer
	# specifying the number of elements of +x+ to use.
	# If +n+ is larger than the length of +x+, then +x+ is resized
	# and padded with zeros.  Otherwise, if +n+ is smaller than the
	# length of +x+, then +x+ is truncated.
	# @param x [Array<Float>, Array<Complex>] The input signal.
	# @param n [Integer, nil] The length of the DFT, defaults to the lengs of +x+.
	# @param window [Array<Float>, Symbol] Premultiply the input signal by the window, eigher an array of length +n+, or a window symbol (ex: :blackman).
	# @param normalize [Boolean] Divide the output by the area under the window.
	# @return [Array<Complex>] The result of the DFT.
	def self.dft (x, n = nil, window: nil, normalize: false)
		xlen = x.length
		if n then
			n = [1, n.to_i].max 
			len = n
			xlen = [xlen, n].min
		else
			len = x.length
		end
		return Roctave.fft(x, n, window: window, normalize: normalize) if Math.log2(len).floor == Math.log2(len) 

		case window
		when Array
			raise ArgumentError.new "The window must have the same size as the input array" if window.length < xlen
			x = (0...xlen).collect{|i| x[i] * window[i]}
		when Symbol
			window = Roctave.send(window, xlen)
			x = (0...xlen).collect{|i| x[i] * window[i]}
		when nil
		else
			raise ArgumentError.new "The window must an array or a symbol"
		end

		if normalize then
			if window then
				factor = window.inject(&:+).to_f
			else
				factor = xlen.to_f
			end
		else
			factor = 1.0
		end

		y = (0...len).collect do |k|
			acc = Complex(0)
			(0...xlen).each do |i|
				acc += Complex.polar(1.0, -2.0*Math::PI*k*i / len) * x[i]
			end
			acc / factor
		end
		y
	end


	##
	# Compute the inverse discrete Fourier transform of +x+.
	# If called with two arguments, +n+ is expected to be an integer
	# specifying the number of elements of +x+ to use.
	# If +n+ is larger than the length of +x+, then +x+ is resized
	# and padded with zeros.  Otherwise, if +n+ is smaller than the
	# length of +x+, then +x+ is truncated.
	# @param x [Array<Float>, Array<Complex>] The input signal.
	# @param n [Integer, nil] The length of the DFT, defaults to the lengs of +x+.
	# @return [Array<Complex>] The result of the DFT.
	def self.idft (x, n = nil)
		xlen = x.length
		if n then
			n = [1, n.to_i].max 
			len = n
			xlen = [xlen, n].min
		else
			len = x.length
		end
		return Roctave.ifft(x, n) if Math.log2(len).floor == Math.log2(len) 


		y = (0...len).collect do |k|
			acc = Complex(0)
			(0...xlen).each do |i|
				acc += Complex.polar(1.0, 2.0*Math::PI*k*i / len) * x[i]
			end
			acc / len
		end
		y
	end


	##
	# same as +dft+, but computes ~30x faster.
	# The length of the FFT is rounded to the neareast power of two.
	# @see dft
	def self.fft(x, n = nil, window: nil, normalize: false)
		if n then
			len = [1, n.to_i].max
		else
			len = x.length
		end
		prevpow2 = 2**(Math.log2(len).floor)
		nextpow2 = 2**(Math.log2(len).ceil)
		if (nextpow2 - len) <= (len - prevpow2) then
			len = nextpow2
		else
			len = prevpow2
		end
		wlen = [x.length, len].min

		case window
		when Array
			raise ArgumentError.new "The window must be of size #{wlen}" unless window.length == wlen
			x = (0...wlen).collect{|i| x[i] * window[i]}
		when Symbol
			window = Roctave.send(window, wlen)
			x = (0...wlen).collect{|i| x[i] * window[i]}
		when nil
		else
			raise ArgumentError.new "The window must be an array or a symbol"
		end

		if x.length != len then
			x = (0...len).collect{|i| x[i].nil? ? 0.0 : x[i]}
		end

		y = iterative_fft(x)

		if normalize then
			if window then
				factor = window.inject(&:+).to_f
			else
				factor = wlen.to_f
			end
			y.collect!{|v| v / factor}
		end

		y
	end


	##
	# Used internaly. Use Roctave.fft instead of calling directly this function.
	# Implements the Cooley-Tukey FFT
	def self.iterative_fft (arr_in)
		## Implementation from https://en.wikipedia.org/wiki/Cooley%E2%80%93Tukey_FFT_algorithm 2019-06-11
		n = arr_in.length
		nbl = Math.log2(n)
		raise ArgumentError.new "Input array must have a length power of two" if nbl.floor != nbl
		nbl = nbl.to_i

		## bit-reverse copy
		arr = (0...n).collect do |i|
			arr_in[i.to_s(2).rjust(nbl, '0').reverse.to_i(2)]
		end

		(1..nbl).each do |s|
			m = 2**s
			wm = Complex.polar(1.0, -2.0*Math::PI/m)
			(0...n).step(m).each do |k|
				w = Complex(1.0)
				(0 ... m/2).each do |j|
					t = w*arr[k+j+m/2]
					u = arr[k+j]
					arr[k+j] = u + t
					arr[k+j+m/2] = u - t
					w = w * wm
				end
			end
		end

		arr
	end


	# @param x [Array]
	# @return [Array]
	def self.fftshift (x)
		xl = x.length
		xx = (xl / 2.0).ceil
		y = Array(xl)
		i = 0
		(xx...xl).each do |j|
			y[i] = x[j]
			i += 1
		end
		(0...xx).each do |j|
			y[i] = x[j]
			i += 1
		end
		y
	end


	##
	# same as +idft+, but computes ~30x faster.
	# The length of the iFFT is rounded to the neareast power of two.
	# @see dft
	def self.ifft(x, n = nil, normalized_input: false)
		if n then
			len = [1, n.to_i].max
		else
			len = x.length
		end
		prevpow2 = 2**(Math.log2(len).floor)
		nextpow2 = 2**(Math.log2(len).ceil)
		if (nextpow2 - len) <= (len - prevpow2) then
			len = nextpow2
		else
			len = prevpow2
		end
		wlen = [x.length, len].min

		if x.length != len then
			x = (0...len).collect{|i| x[i].nil? ? 0.0 : x[i]}
		end

		y = iterative_ifft(x)

		unless normalized_input then
			y.collect!{|v| v / len}
		end

		y
	end


	##
	# Used internaly. Use Roctave.fft instead of calling directly this function.
	# Implements the Cooley-Tukey FFT
	def self.iterative_ifft (arr_in)
		## Implementation from https://en.wikipedia.org/wiki/Cooley%E2%80%93Tukey_FFT_algorithm 2019-06-11
		n = arr_in.length
		nbl = Math.log2(n)
		raise ArgumentError.new "Input array must have a length power of two" if nbl.floor != nbl
		nbl = nbl.to_i

		## bit-reverse copy
		arr = (0...n).collect do |i|
			arr_in[i.to_s(2).rjust(nbl, '0').reverse.to_i(2)]
		end

		(1..nbl).each do |s|
			m = 2**s
			wm = Complex.polar(1.0, 2.0*Math::PI/m)
			(0...n).step(m).each do |k|
				w = Complex(1.0)
				(0 ... m/2).each do |j|
					t = w*arr[k+j+m/2]
					u = arr[k+j]
					arr[k+j] = u + t
					arr[k+j+m/2] = u - t
					w = w * wm
				end
			end
		end

		arr
	end
end


## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	# @!group Windows

	##
	# Hann window
	# @param n [Integer] Length of the window
	# @return [Array<Float>]
	def self.hann (n)
		n = [1, n.to_i].max
		t = n - 1
		(0...n).collect{|i| 0.5 - 0.5*Math.cos(2.0*Math::PI*i/t)}
	end


	##
	# Hamming window
	# @param n [Integer] Length of the window
	# @return [Array<Float>]
	def self.hamming (n)
		n = [1, n.to_i].max
		t = n - 1
		(0...n).collect{|i| 0.54 - 0.46*Math.cos(2.0*Math::PI*i/t)}
	end


	##
	# Blackman window
	# @param n [Integer] Length of the window
	# @param alpha [Float]
	# @return [Array<Float>]
	def self.blackman (n, alpha = 0.16)
		n = [1, n.to_i].max
		t = n - 1
		#(0...n).collect{|i| 0.42 - 0.5*Math.cos(2.0*Math::PI*i/t) + 0.08*Math.cos(4.0*Math::PI*i/t)}
		a0 = (1.0 - alpha) / 2.0
		a1 = 0.5
		a2 = alpha / 2.0
		(0...n).collect{|i| a0 - a1*Math.cos(2.0*Math::PI*i/t) + a2*Math.cos(4.0*Math::PI*i/t)}
	end


	##
	# Nuttall window
	# @param n [Integer] Length of the window
	# @return [Array<Float>]
	def self.nuttall (n)
		n = [1, n.to_i].max
		t = n - 1
		a0 = 0.355768
		a1 = 0.487396
		a2 = 0.144232
		a3 = 0.012604
		(0...n).collect{|i| a0 - a1*Math.cos(2.0*Math::PI*i/t) + a2*Math.cos(4.0*Math::PI*i/t) - a3*Math.cos(6.0*Math::PI*i/t)}
	end


	##
	# Blackman-Nuttall window
	# @param n [Integer] Length of the window
	# @return [Array<Float>]
	def self.blackman_nuttall (n)
		n = [1, n.to_i].max
		t = n - 1
		a0 = 0.3635819
		a1 = 0.4891775
		a2 = 0.1365995
		a3 = 0.0106411
		(0...n).collect{|i| a0 - a1*Math.cos(2.0*Math::PI*i/t) + a2*Math.cos(4.0*Math::PI*i/t) - a3*Math.cos(6.0*Math::PI*i/t)}
	end


	##
	# Blackman-Harris window
	# @param n [Integer] Length of the window
	# @return [Array<Float>]
	def self.blackman_harris (n)
		n = [1, n.to_i].max
		t = n - 1
		a0 = 0.35875
		a1 = 0.48829
		a2 = 0.14128
		a3 = 0.01168
		(0...n).collect{|i| a0 - a1*Math.cos(2.0*Math::PI*i/t) + a2*Math.cos(4.0*Math::PI*i/t) - a3*Math.cos(6.0*Math::PI*i/t)}
	end


	##
	# Gaussian window
	# @param n [Integer] Length of the window
	# @param sigma [Float] The standard deviation of the Gaussian function is sigma*N/2 sampling periods.
	# @return [Array<Float>]
	def self.gaussian (n, sigma = 0.4)
		n = [1, n.to_i].max
		t = n - 1
		(0...n).collect{|i| Math.exp(-0.5*((i - t/2.0)/(sigma*t/2.0))**2)}
	end
end


## Copyright (C) 2000 Paul Kienzle <pkienzle@users.sf.net> (implementation in octave)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	# @!group FIR filters

	##
	# Frequency sampling-based FIR filter design.
	#
	# Produce an order N FIR filter with arbitrary frequency response M
	# over frequency bands F, returning the N+1 filter coefficients in B.
	# The vector F specifies the frequency band edges of the filter
	# response and M specifies the magnitude response at each frequency.
	# 
	# The vector F must be nondecreasing over the range [0,1], and the
	# first and last elements must be 0 and 1, respectively.  A
	# discontinuous jump in the frequency response can be specified by
	# duplicating a band edge in F with different values in M.
	# 
	# The resolution over which the frequency response is evaluated can
	# be controlled with the GRID_N argument.  The default is 512 or the
	# next larger power of 2 greater than the filter length.
	# 
	# The band transition width for discontinuities can be controlled
	# with the RAMP_N argument.  The default is GRID_N/25.  Larger values
	# will result in wider band transitions but better stopband
	# rejection.
	# 
	# An optional shaping WINDOW can be given as a vector with length
	# N+1, or a symbol (ex: :blackman).
	# If not specified, a Hamming window of length N+1 is used.
	# @param n [Integer] The filter order
	# @param f [Array<Float>] Frequency bands
	# @param m [Array<Numeric>] Magnitude at frequency bands, can be complex numbers.
	# @param type [nil, :odd_symmetry, :even_symmetry] Specify the symmetry of the filter. nil and :even_symmetry are the same and default, for type 1 and 2 filters, :odd_symmetry is for type 3 and 4 filters.
	# @param grid [Integer] The length of the grid over which the frequency response is evaluated. Defaults to 512 or the next larger power of 2 greater than the filter length.
	# @param ramp [Integer, Float] The band transition width for discontinuities, expressed in percentage relative to the niquist frequency.
	# @param window [Array<Float>, Symbol] The window to use, either an array of length N+1, or a symbol. Defaults to :hamming
	# @return [Array<Float>] The filter coefficients B
	def self.fir2 (n, f, m, type = nil, grid: nil, ramp: nil, window: :hamming)
		## Taken from Octave /usr/share/octave/packages/signal-1.3.2/fir2.m
		f = f.map{|e| e}
		m = m.map{|e| e}
		n = [1, n.to_i].max
		len = n + 1

		raise ArgumentError.new "Argument 2 must be an array" unless f.kind_of?(Array)
		raise ArgumentError.new "Argument 3 must be an array" unless m.kind_of?(Array)
		case type
		when nil
			type = :even_symmetry
		when :even_symmetry
		when :odd_symmetry
		else
			raise ArgumentError.new "Argument 4 (type) must either be nil, :odd_symmetry or :even_symmetry" unless m.kind_of?(Array)
		end

		case window
		when Array
			raise ArgumentError.new "Window array must be of length #{len}" unless window.length == len
		when Symbol
			window = self.send(window, len)
		when nil
		else
			raise ArgumentError.new "Window must either be an array of length N+1 or the symbol of a window"
		end

		if grid then
			grid = [((n+1)/2.0).ceil.to_i, grid.to_i].max 
		else
			grid = 512 
			grid = (2**Math.log2(len).ceil).to_i if grid < len
		end

		if ramp then
			ramp = ramp.abs 
		else
			ramp = 4
		end
		ramp = ramp.to_f / 200.0

		if f.length < 2 or f[0] != 0.0 or f[-1] != 1.0 or (1 ... f.length).collect{|i| (f[i] - f[i-1]) < 0.0}.include?(true) then
			raise ArgumentError.new "frequency must be nondecreasing starting from 0 and ending at 1"
		elsif f.length != m.length then
			raise ArgumentError.new "frequency and magnitude arrays must be the same length"
		end


		if type == :odd_symmetry and m.find{|v| v.is_a?(Complex)}.nil? then
			m.collect!{|v| Complex(0, -v)}
		end


		## Apply ramps to discontinuities
		if ramp > 0 then
			## Remember the original frequency points prior to applying ramp
			basef = f.collect{|e| e}
			basem = m.collect{|e| e}

			## Separate identical frequencies, but keep the midpoints
			idx = (1 ... f.length).collect{|i| ((f[i] - f[i-1]) == 0.0) ? i-1 : nil}.reject(&:nil?)
			idx.each do |i|
				f[i]   -= ramp
				f[i+1] += ramp
			end
			## Make sure the frequency points stay monotonic in [0, 1]
			f = (f + basef).collect{|e| [1.0, [0.0, e].max].min}.uniq.sort

			## Preserve window shape even though f may have changed
			m = f.collect.with_index do |framped, i|
				leftmost = nil
				leftmost_index = nil
				basef.each.with_index do |e, j|
					if (e < framped) or (e == framped and leftmost != framped) then
						leftmost = e
						leftmost_index = j
					end
				end

				rightmost = nil
				rightmost_index = nil
				basef.reverse.each.with_index do |e, j|
					if (e > framped) or (e == framped and rightmost != framped) then
						rightmost = e
						rightmost_index = basef.length - 1 - j
					end
				end

				if leftmost_index > rightmost_index then
					leftmost,rightmost = rightmost,leftmost
					leftmost_index,rightmost_index = rightmost_index,leftmost_index
				end

				#puts "[#{i}]  #{leftmost_index} - #{rightmost_index}"

				ml = basem[leftmost_index]
				mr = basem[rightmost_index]

				if (rightmost - leftmost) == 0 then
					(ml + mr) / 2.0
				else
					ml*(rightmost - framped)/(rightmost - leftmost) + mr*(framped - leftmost)/(rightmost - leftmost)
				end
			end
			#Roctave.plot(basef, basem, '-or;original;', f, m, '-*b;ramped;', xlim: (-0.1 .. 1.1), ylim: (-0.1 .. 1.1), grid: true, title: 'Ramp')
		end

		## Interpolate between grid points
		fgrid = Roctave.linspace(0.0, 1.0, grid + 1)
		mgrid = fgrid.collect.with_index do |fg, i|
			leftmost = nil
			leftmost_index = nil
			f.each.with_index do |e, j|
				if (e < fg) or (e == fg and leftmost != e) then
					leftmost = e
					leftmost_index = j
				end
			end

			rightmost = nil
			rightmost_index = nil
			f.reverse.each.with_index do |e, j|
				if (e > fg) or (e == fg and rightmost != e) then
					rightmost = e
					rightmost_index = f.length - 1 - j
				end
			end

			if leftmost_index > rightmost_index then
				leftmost,rightmost = rightmost,leftmost
				leftmost_index,rightmost_index = rightmost_index,leftmost_index
			end

			ml = m[leftmost_index]
			mr = m[rightmost_index]

			if (rightmost - leftmost) == 0 then
				(ml + mr) / 2.0
			else
				ml*(rightmost - fg)/(rightmost - leftmost) + mr*(fg - leftmost)/(rightmost - leftmost)
			end
		end


		## Transform frequency response into time response and
		## center the response about n/2, truncating the excess
		if type == :even_symmetry then
			if (n & 1) == 0 then # Type I
				#puts 'Type I'
				b = mgrid + mgrid[1...grid].reverse
				b = idft(b)
				mid = (n+1) / 2.0
				b = (b[-(mid.floor) .. -1] + b[0 ... mid.ceil]).collect{|e| e.real}
			else # Type II
				#puts 'Type II'
				## Add zeros to interpolate by 2, then pick the odd values below
				b = mgrid + Array.new(grid*2){0.0} + mgrid[1 ... grid].reverse
				b = idft(b)
				b = (((b.length - n) ... b.length).step(2).to_a + (1 .. n).step(2).to_a).collect{|i| b[i].real * 2.0}
			end
		else
			if (n & 1) == 0 then # Type III
				#puts 'Type III'
				b = mgrid + mgrid[1...grid].conj.reverse
				####
				#br = b.collect{|v| v.real}
				#bi = b.collect{|v| v.imag}
				#t = (-b.length/2...b.length/2).to_a
				#plot(t, br, ';Real;', t, bi, ';Imaginary;')
				####
				b = idft(b)
				mid = (n+1) / 2.0
				b = (b[-(mid.floor) .. -1] + b[0 ... mid.ceil]).collect{|e| e.real}
			else # Type IV
				#puts 'Type IV'
				## Add zeros to interpolate by 2, then pick the odd values below
				b = mgrid + Array.new(grid*2){0.0} + mgrid.conj[1 ... grid].reverse
				b = idft(b)
				b = (((b.length - n) ... b.length).step(2).to_a + (1 .. n).step(2).to_a).collect{|i| b[i].real * 2.0}
			end
		end

		b.collect!.with_index{|e, i| e*window[i]} if window

		#Roctave.plot(f, m, '-or;original;', fgrid, mgrid, '-xb;grid;', Roctave.linspace(0, 1, 1024), Roctave.dft(b, 2048).abs[0...1024], 'g;response;', xlim: (-0.1 .. 1.1), ylim: (-0.1 .. 1.1), grid: true, title: 'Grid')

		b
	end
end


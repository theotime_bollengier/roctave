## Copyright (C) 1994-2015 John W. Eaton (GNU Octave implementation)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave

	# @!group Plotting

	##
	# Return the complex frequency response H of the rational IIR filter
	# whose numerator and denominator coefficients are B and A,
	# respectively.
	#
	# If A is omitted, the denominator is assumed to be 1 (this
	# corresponds to a simple FIR filter).
	#
	# @param b [Array<Numeric>] Numerator of the IIR filter.
	# @param a [Array<Numeric>] Denominator of the IIR filter.
	# @param nb_points [Integer] Number of points on which to evaluate the response. Faster if power of two.
	# @param region [Symbol] Either :half of :whole
	# @param fs [Float] If not specified, output frequencies are on [0 pi] or [-pi pi] depending on +region+
	# @param opts [Symbols] You can specify :magnitude, :phase and :group_delay to plot the magnitude, phase and group delay response, and specify :degree and :dB, and :shift for fftshifting when ploting the whole region
	def self.freqz (b, *opts, nb_points: 512, region: nil, fs: nil)
		nb_points = [4, nb_points.to_i].max
		fs = fs.to_f.abs if fs
		sampling_rate = fs
		normalize_freqency = fs.nil?
		degree = false
		log = false
		plot_m = false
		plot_p = false
		plot_d = false
		shift = false
		a = [1.0]
		opts.each do |opt|
			case opt
			when :magnitude
				plot_m = true
			when :phase
				plot_p = true
			when :group_delay
				plot_d = true
			when :degree
				degree = true
			when :dB
				log = true
			when :shift
				shift = true
			when Array
				a = opt
			else
				raise ArgumentError.new "Unexpected argument \"#{opt}\""
			end
		end
		do_plot = (plot_m or plot_p)
		raise ArgumentError.new "First argument must be an array" unless b.kind_of?(Array)
		b = [1.0] if b.empty?
		if region != :half and region != :whole then
			if b.find{|v| v.kind_of?(Complex)} or a.find{|v| v.kind_of?(Complex)} then
				region = :whole 
			else
				region = :half 
			end
		end
		if fs.nil? then
			fs = 2*Math::PI
		end

		k = [b.length, a.length].max
		if k > nb_points/2 and plot_p then
			## Ensure a causal phase response.
			nb_points = nb_points * 2**Math.log2(2.0*k/nb_points).ceil
		end

		if region == :whole then
			####
=begin
				n = nb_points
				if do_plot then
					f = (0..nb_points).collect{|i| fs * i / n} # do 1 more for the plot
				else
					f = (0...nb_points).collect{|i| fs * i / n}
				end
=end
			####
			n = nb_points
			f = (0...nb_points).collect{|i| fs * i / n}
			####
		else
			n = 2*nb_points
			#nb_points += 1 if do_plot
			f = (0...nb_points).collect{|i| fs * i / n}
		end

		pad_sz = n*(k.to_f / n).ceil
		b = Roctave.postpad(b, pad_sz)
		a = Roctave.postpad(a, pad_sz)

		hb = Roctave.zeros(nb_points)
		ha = Roctave.zeros(nb_points)

		(0...pad_sz).step(n).each do |i|
			tmp = Roctave.dft(Roctave.postpad(b[i ... i+n], n))[0...nb_points]
			hb = hb.collect.with_index{|v, j| v+tmp[j]}
			tmp = Roctave.dft(Roctave.postpad(a[i ... i+n], n))[0...nb_points]
			ha = ha.collect.with_index{|v, j| v+tmp[j]}
		end

		h = (0...nb_points).collect{|i| hb[i] / ha[i]}

		if do_plot and Roctave.respond_to?(:plot) then
			fs /= 2.0 if region == :half
			if plot_m then
				title = '{/:Bold Magnitude response}'
				if normalize_freqency then
					xlabel = 'Normalized frequency (x {/Symbol p} rad/sample)'
					if region == :half then
						freq = f.collect{|v| v/fs}
						xlim = [0, 1]
					else
						freq = f.collect{|v| 2.0*v/fs}
						xlim = [0, 2]
					end
				else
					xlabel = 'Frequency'
					freq = f
					xlim = [0, fs]
				end
				if log then
					ylabel = 'Magnitude (dB)'
					mag = h.collect{|v| 20*Math.log10(v.abs)}
				else
					ylabel = 'Magnitude'
					mag = h.collect{|v| v.abs}
				end
				if region == :whole and shift then
					xlim = xlim.collect{|v| v - xlim.last / 2.0}
					offset = (freq[1] - freq[0])/2.0
					freq = freq.collect{|v| v - freq.last / 2.0 - offset}
					mag = Roctave.fftshift(mag)
				end
				Roctave.plot(freq, mag, title: title, xlabel: xlabel, ylabel: ylabel, xlim: xlim, grid: true)
			end
			if plot_p then
				title = '{/:Bold Phase shift response}'
				if normalize_freqency then
					xlabel = 'Normalized frequency (x {/Symbol p} rad/sample)'
					if region == :half then
						freq = f.collect{|v| v/fs}
						xlim = [0, 1]
					else
						freq = f.collect{|v| 2.0*v/fs}
						xlim = [0, 2]
					end
				else
					xlabel = 'Frequency'
					freq = f
					xlim = [0, fs]
				end
				pha = h.collect{|v| v.arg}
				if region == :whole and shift then
					xlim = xlim.collect{|v| v - xlim.last / 2.0}
					offset = (freq[1] - freq[0])/2.0
					freq = freq.collect{|v| v - freq.last / 2.0 - offset}
					pha = Roctave.fftshift(pha)
				end
				pha = Roctave.unwrap(pha)
				if degree then
					ylabel = 'Phase (degree)'
					pha = pha.collect{|v| v / Math::PI * 180.0}
				else
					ylabel = 'Phase (radians)'
				end
				Roctave.plot(freq, pha, title: title, xlabel: xlabel, ylabel: ylabel, xlim: xlim, grid: true)
			end
			if plot_d then
				title = '{/:Bold Group delay}'
				if normalize_freqency then
					xlabel = 'Normalized frequency (x {/Symbol p} rad/sample)'
					ylabel = 'Group delay (in sample time)'
					if region == :half then
						freq = f.collect{|v| v/fs}
						xlim = [0, 1]
					else
						freq = f.collect{|v| 2.0*v/fs}
						xlim = [0, 2]
					end
				else
					xlabel = 'Frequency'
					freq = f
					xlim = [0, fs]
					ylabel = 'Group delay (s)'
				end
				pha = h.collect{|v| v.arg}
				if region == :whole and shift then
					xlim = xlim.collect{|v| v - xlim.last / 2.0}
					offset = (freq[1] - freq[0])/2.0
					freq = freq.collect{|v| v - freq.last / 2.0 - offset}
					pha = Roctave.fftshift(pha)
				end
				pha = Roctave.unwrap(pha)
				dd = 2.0*(f.last - f.first) / (f.length - 1)
				dd *= 2.0*Math::PI unless normalize_freqency
				gd = Array.new(pha.length){0.0}
				(1...pha.length-1).each do |i|
					gd[i] = (pha[i-1] - pha[i+1]) / dd 
				end
				gd[0] = gd[1]
				gd[-1] = gd[-2]
				Roctave.plot(freq, gd, title: title, xlabel: xlabel, ylabel: ylabel, xlim: xlim, grid: true)
			end
		end

		[h, f]
	end

end


## Copyright (C) 1994-2015 John W. Eaton (GNU Octave implementation)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave

	##
	# Returns the coefficients of the polynomial whose roots are the elements of the input array.
	# @param x [Array<Numeric>] An array containing the roots of a polynomial
	# @return [Array<Numeric>] An array containing coefficients of a polynomial whose roots are +x+.
	def self.poly (x)
		m = x.length

		if m == 0 then
			return [1.0]
		else
			v = x
		end

		y = Array.new(m+1){0.0}
		y[0] = 1.0
		(1..m).each do |j|
			tmp = (1..j).collect{|i| y[i] - v[j-1]*y[i-1]}
			y[1..j] = tmp
		end

		y
	end

end


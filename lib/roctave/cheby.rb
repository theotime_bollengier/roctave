## Copyright (C) 1999 Paul Kienzle <pkienzle@users.sf.net> (GNU Octave implementation)
## Copyright (C) 2003 Doug Stewart <dastew@sympatico.ca> (GNU Octave implementation)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	# @!group IIR filters

	##
	# Generate a Chebyshev type I filter with rp dB of passband ripple.
	# @param n [Integer] The filter order
	# @param rp [Float] Peak-to-peak passband ripple. If positive, in dB, if negative, rp_dB = -20*log10(1-rp)
	# @param wc [Float, Array<Float>, Range] The cutoff frequency normalized to [0 1], or the two band edges as an array or a range
	# @param type [Symbol] Either :low, :high, :pass or :stop
	# @return [Array<Array{Float}>] An array containing the numerator and denominator arrays of polynomial coefficients [b, a]
	def self.cheby1 (n, rp, wc, type = :low)
		n = [1, n.to_i].max

		case wc
		when Numeric
			wc = [[0.0, [1.0, wc.to_f].min].max]
		when Range
			wc = [[0.0, [1.0, wc.begin.to_f].min].max, [0.0, [1.0, wc.end.to_f].min].max].sort
		when Array
			raise ArgumentError.new "Argument 3 must have exactly two elements" unless wc.length == 2
			wc = [[0.0, [1.0, wc.first.to_f].min].max, [0.0, [1.0, wc.last.to_f].min].max].sort
		else
			raise ArgumentError.new "Argument 3 must be a numeric, a two numeric array or a range"
		end
		wc = wc.uniq
		raise ArgumentError.new "All elemnts of W must be in the range [0, 1]" if wc.find{|v| v < 0.0 or v > 1.0}

		stop = false
		case type
		when :low
			stop = false
		when :pass
			stop = false
		when :high
			stop = true
		when :stop
			stop = true
		else
			raise ArgumentError.new "Argument 4 must be :low, :pass, :high or :stop"
		end

		raise ArgumentError.new "cheby1: passband ripple RP must be a non-zero scalar" unless (rp.is_a?(Float) and rp != 0.0)
		rp = -20.0*Math.log10(1.0 + rp) if rp < 0.0

		## Prewarp to the band edges to s plane
		t = 2.0 # sampling frequency of 2 Hz
		wc.collect!{|v| 2.0 / t * Math.tan(Math::PI * v / t)}

		## Generate splane poles and zeros for the Chebyshev type 1 filter
		c = 1.0  ## default cutoff frequency
		epsilon = Math.sqrt(10**(rp / 10.0) - 1.0)
		v0 = Math.asinh(1.0 / epsilon) / n
		pole_s = (-(n-1) .. (n-1)).step(2).collect{|i| Complex.polar(1.0, Math::PI*i/(2.0*n))}
		pole_s.collect!{|v| Complex(-Math.sinh(v0) * v.real, Math.cosh(v0) * v.imag)}
		zero_s = []

		## compensate for amplitude at s=0
		gain_s = pole_s.inject(1.0){|m, v| m * (-v)}
		## if n is even, the ripple starts low, but if n is odd the ripple
		## starts high. We must adjust the s=0 amplitude to compensate.
		gain_s = gain_s / 10**(rp / 20.0) if (n & 1) == 0

		## S-plane frequency transform
		zero_s, pole_s, gain_s = Roctave.sftrans(zero_s, pole_s, gain_s, wc, stop)

		## Use bilinear transform to convert poles to the Z plane
		zero_z, pole_z, gain_z = Roctave.bilinear(zero_s, pole_s, gain_s, t)

		## Convert to the correct output form
		b = Roctave.poly(zero_z).collect{|v| (gain_z * v).real}
		a = Roctave.poly(pole_z).collect{|v| v.real}

		return [b, a]
	end


	##
	# Generate a Chebyshev type II filter with rs dB of stopband ripple.
	# @param n [Integer] The filter order
	# @param rs [Float] Peak-to-peak stopband ripple. If positive, in dB, if negative, rp_dB = -20*log10(1-rs)
	# @param wc [Float, Array<Float>, Range] The cutoff frequency normalized to [0 1], or the two band edges as an array or a range
	# @param type [Symbol] Either :low, :high, :pass or :stop
	# @return [Array<Array{Float}>] An array containing the numerator and denominator arrays of polynomial coefficients [b, a]
	def self.cheby2 (n, rs, wc, type = :low)
		n = [1, n.to_i].max

		case wc
		when Numeric
			wc = [[0.0, [1.0, wc.to_f].min].max]
		when Range
			wc = [[0.0, [1.0, wc.begin.to_f].min].max, [0.0, [1.0, wc.end.to_f].min].max].sort
		when Array
			raise ArgumentError.new "Argument 3 must have exactly two elements" unless wc.length == 2
			wc = [[0.0, [1.0, wc.first.to_f].min].max, [0.0, [1.0, wc.last.to_f].min].max].sort
		else
			raise ArgumentError.new "Argument 3 must be a numeric, a two numeric array or a range"
		end
		wc = wc.uniq
		raise ArgumentError.new "All elemnts of W must be in the range [0, 1]" if wc.find{|v| v < 0.0 or v > 1.0}

		stop = false
		case type
		when :low
			stop = false
		when :pass
			stop = false
		when :high
			stop = true
		when :stop
			stop = true
		else
			raise ArgumentError.new "Argument 4 must be :low, :pass, :high or :stop"
		end

		raise ArgumentError.new "cheby2: passband ripple RS must be a non-zero scalar" unless (rs.is_a?(Float) and rs != 0.0)
		rs = -20.0*Math.log10(-rs) if rs < 0.0

		## Prewarp to the band edges to s plane
		t = 2.0 # sampling frequency of 2 Hz
		wc.collect!{|v| 2.0 / t * Math.tan(Math::PI * v / t)}

		## Generate splane poles and zeros for the Chebyshev type 2 filter
		## From: Stearns, SD; David, RA; (1988). Signal Processing Algorithms.
		##       New Jersey: Prentice-Hall.
		c = 1.0  ## default cutoff frequency
		lmbd = 10.0**(rs/20.0)
		phi = Math.log(lmbd + Math.sqrt(lmbd**2 - 1)) / n
		theta = (1..n).collect{|i| Math::PI*(i-0.5)/n}
		alpha = theta.collect{|v| -Math.sinh(phi)*Math.sin(v)}
		beta = theta.collect{|v| Math.cosh(phi)*Math.cos(v)}
		if (n & 1) == 1 then
			## drop theta==pi/2 since it results in a zero at infinity
			zero_s = ((0...(n-1)/2).to_a + ((n+3)/2-1 ... n).to_a).collect{|i| Complex(0.0, c/Math.cos(theta[i]))}
		else
			zero_s = theta.collect{|v| Complex(0.0, c/Math.cos(v))}
		end
		pole_s = (0...n).collect{|i| c/(alpha[i]**2 + beta[i]**2)*Complex(alpha[i], -beta[i])}

		## Compensate for amplitude at s=0
		## Because of the vagaries of floating point computations, the
		## prod(pole)/prod(zero) sometimes comes out as negative and
		## with a small imaginary component even though analytically
		## the gain will always be positive, hence the abs(real(...))
		gain_s = (pole_s.inject(&:*) / zero_s.inject(&:*)).real.abs

		## S-plane frequency transform
		zero_s, pole_s, gain_s = Roctave.sftrans(zero_s, pole_s, gain_s, wc, stop)

		## Use bilinear transform to convert poles to the Z plane
		zero_z, pole_z, gain_z = Roctave.bilinear(zero_s, pole_s, gain_s, t)

		## Convert to the correct output form
		b = Roctave.poly(zero_z).collect{|v| (gain_z * v).real}
		a = Roctave.poly(pole_z).collect{|v| v.real}

		return [b, a]
	end

end


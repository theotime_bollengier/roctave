## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	class FirFilter
		include Roctave::Filter

		# @see Roctave.freqz
		def freqz (*args)
			Roctave.freqz(numerator, *args)
		end

		# @see Roctave.stem
		def stem (*args)
			Roctave.stem(numerator, *args)
		end

		# @see Roctave.zplane
		def zplane
			Roctave.zplane(numerator)
		end

		# @return [Roctave::FirFilter]
		def clone
			Roctave::FirFilter.new numerator
		end

		# Matlab-like +fir1+ function to generate a FIR filter.
		# @see Roctave.fir1
		# @return [Roctave::FirFilter]
		def self.fir1 (*args)
			Roctave::FirFilter.new Roctave.fir1(*args)
		end

		# Matlab-like +fir2+ function to generate a FIR filter.
		# @see Roctave.fir2
		# @return [Roctave::FirFilter]
		def self.fir2 (*args)
			Roctave::FirFilter.new Roctave.fir2(*args)
		end

		# Returns a low-pass FIR filter.
		# @see Roctave.fir_low_pass
		# @return [Roctave::FirFilter]
		def self.low_pass (*args)
			Roctave::FirFilter.new Roctave.fir_low_pass(*args)
		end

		# Returns a high-pass FIR filter.
		# @see Roctave.fir_high_pass
		# @return [Roctave::FirFilter]
		def self.high_pass (*args)
			Roctave::FirFilter.new Roctave.fir_high_pass(*args)
		end

		# Returns a band-pass FIR filter.
		# @see Roctave.fir_band_pass
		# @return [Roctave::FirFilter]
		def self.band_pass (*args)
			Roctave::FirFilter.new Roctave.fir_band_pass(*args)
		end

		# Returns a band-stop FIR filter.
		# @see Roctave.fir_band_stop
		# @return [Roctave::FirFilter]
		def self.band_stop (*args)
			Roctave::FirFilter.new Roctave.fir_band_stop(*args)
		end

		# Returns a Hilbert transformer FIR filter.
		# @see Roctave.fir_hilbert
		# @return [Roctave::FirFilter]
		def self.hilbert (*args)
			Roctave::FirFilter.new Roctave.fir_hilbert(*args)
		end

		# Returns a discriminator FIR filter.
		# @see Roctave.fir_differentiator
		# @return [Roctave::FirFilter]
		def self.differentiator (*args)
			Roctave::FirFilter.new Roctave.fir_differentiator(*args)
		end

		# Returns a kind of discriminator FIR filter.
		# @see Roctave.finite_difference_coefficients
		# @return [Roctave::FirFilter]
		def self.finite_difference_coefficients (*args)
			Roctave::FirFilter.new Roctave.finite_difference_coefficients(*args)
		end

		# Least square method to generate a FIR filter.
		# @see Roctave.firls
		# @return [Roctave::FirFilter]
		def self.firls (*args)
			Roctave::FirFilter.new Roctave.firls(*args)
		end

		# Least Parks-McClellan method to generate a FIR filter.
		# @see Roctave.firpm
		# @return [Roctave::FirFilter]
		def self.firpm (*args)
			Roctave::FirFilter.new Roctave.firpm(*args)
		end
	end
end


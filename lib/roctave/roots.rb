## Copyright (C) 1994-2015 John W. Eaton (GNU Octave implementation)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave

	##
	# Compute the roots of the polynomial C.
	#
	# For a vector C with N components, return the roots of the
	# polynomial
	# c(1) * x^(N-1) + ... + c(N-1) * x + c(N)
	#
	# @param v [Array<Numeric>] Coefficients of the polynomial. First element is the highest order
	# @return [Array<Numeric>] Array containing the roots.
	def self.roots (v)
		n = v.length

		## If v = [ 0 ... 0 v(k+1) ... v(k+l) 0 ... 0 ],
		## we can remove the leading k zeros,
		## and n - k - l roots of the polynomial are zero.

		return [] if v.empty?

		f = []
		v.each_with_index do |e, i| 
			f << i unless e.abs == 0.0
		end
		m = f.length
		return [] if f.empty?
				
		v = v[f.first .. f.last]
		l = v.length

		if l > 1 then
			a = Matrix.build(l-1, l-1) do |row, col|
				if row == col + 1 then
					1.0
				elsif row == 0 then
					-v[1+col].to_f / v[0]
				else
					0.0
				end
			end
			d = a.eigen.d
			r = (0...l-1).collect{|i| d[i, i]}
			if (f[-1] + 1) < n then
				r = r + Roctave.zeros(n - f[-1] - 1)
			end
		else
			r = Roctave.zeros(n - f[-1] - 1)
		end

		r
	end

end


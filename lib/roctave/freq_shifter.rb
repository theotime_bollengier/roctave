## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	class FreqShifter

		# Multiply the signal with exp(2*i*pi*f*t)
		# @overload shift(signal, freq: 0.5, fs: 1.0, initial_phase: 0.0)
		# @param signal [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
		# @param freq [Float] the amount of frequency to shift
		# @param fs [Float] the sampling frequency
		# @param initial_phase [Float] the initial phase of the oscillator
		# @return [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
		def self.shift (sig, *args)
			shifter = Roctave::FreqShifter.new(*args)
			shifter.shift sig
		end

		# Multiply the signal with cos(2*i*pi*f*t)
		# @overload shift(signal, freq: 0.5, fs: 1.0, initial_phase: 0.0)
		# @param signal [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
		# @param freq [Float] the amount of frequency to shift
		# @param fs [Float] the sampling frequency
		# @param initial_phase [Float] the initial phase of the oscillator
		# @return [Array<Float,Complex>, Float, Complex] a single or and array of float or complex numbers
		def self.amplitude_modulate (sig, *args)
			shifter = Roctave::FreqShifter.new(*args)
			shifter.amplitude_modulate sig
		end
	end
end


## Copyright (C) 2000 Paul Kienzle <pkienzle@users.sf.net> (GNU Octave implementation)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	# @!group FIR filters
	
	##
	# Finite difference equations enable you to take derivatives of any order
	# at any point using any given sufficiently large selection of points.
	# By inputting the locations of your sampled points,
	# this function generates a finite difference equation which will 
	# approximate the derivative at any desired location. 
	#
	# Taken from 
	# https://en.wikipedia.org/wiki/Finite_difference_coefficient
	# and
	# http://web.media.mit.edu/~crtaylor/calculator.html
	#
	# @param s [Array<Float>, Range] Location of sample points, given as an array ex: [-3, -1, 0, 2], or a range ex: (-2 ... 1) which is equivalent to [-2, -1, 0, 1]
	# @param d [Integer] Derivative order
	# @param fs [Float] Sampling frequency
	# @return [Array<Float>] The coefficients
	def self.finite_difference_coefficients (s, d = 1, fs = 1.0)
		case s
		when Range
			s = Range.new([s.begin, s.end].min.to_i, [s.begin, s.end].max.to_i).to_a.collect{|e| e.to_f}
		when Array
			s = s.collect{|e| e.to_f}.sort.uniq
		else
			raise ArgumentError.new "Location of sample points must be an array of integers, or a range"
		end
		d = [1, d.to_i].max
		fs = fs.to_f
		len = s.length 
		raise ArgumentError.new "Please enter a derivative order that is less than the number of points in your stencil." if d >= len 

		y = Matrix.build(len, 1) do |row, col|
			if row == d then
				## d! / h^d
				(1..d).inject(&:*) * fs**d
			else
				0.0
			end
		end

		a = Matrix.build(len) do |row, col|
			s[col]**row
		end

		a_inv = a.inverse

		x = a_inv * y

		x.to_a.flatten.reverse
	end
end


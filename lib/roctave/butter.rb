## Copyright (C) 1999 Paul Kienzle <pkienzle@users.sf.net> (GNU Octave implementation)
## Copyright (C) 2003 Doug Stewart <dastew@sympatico.ca> (GNU Octave implementation)
## Copyright (C) 2011 Alexander Klein <alexander.klein@math.uni-giessen.de> (GNU Octave implementation)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	# @!group IIR filters

	##
	# Generate a Butterworth filter.
	# @param n [Integer] Filter order
	# @param w [Float, Array<Float, Float>, Range] Normalized to [0 1] cutoff frequency. Either a single frequency, or an array of two frequencies for a pass/stop-band or a range.
	# @param type [Symbol] Either :low, :high, :pass, :stop
	# @return [Array<Array{Float}>] An array containing the b and a arrays of floats [b, a].
	def self.butter(n, w, type = :low)
		n = [1, n.to_i].max

		case w
		when Numeric
			w = [[0.0, [1.0, w.to_f].min].max]
		when Range
			w = [[0.0, [1.0, w.begin.to_f].min].max, [0.0, [1.0, w.end.to_f].min].max].sort
		when Array
			raise ArgumentError.new "Argument 2 must have exactly two elements" unless w.length == 2
			w = [[0.0, [1.0, w.first.to_f].min].max, [0.0, [1.0, w.last.to_f].min].max].sort
		else
			raise ArgumentError.new "Argument 2 must be a numeric, a two numeric array or a range"
		end

		stop = false
		case type
		when :low
			stop = false
		when :pass
			stop = false
		when :high
			stop = true
		when :stop
			stop = true
		else
			raise ArgumentError.new "Argument 3 must be :low, :pass, :high or :stop"
		end

		## Prewarp to the band edges to s plane
		t = 2.0 # sampling frequency of 2 Hz
		w.collect!{|v| 2.0 / t * Math.tan(Math::PI * v / t)}

		## Generate splane poles for the prototype Butterworth filter
		## source: Kuc
		c = 1.0 # default cutoff frequency
		pole_s = (1..n).collect{|i| c * Complex.polar(1.0, Math::PI * (2.0 * i + n - 1) / (2 * n))}
		if (n & 1) == 1 then
			pole_s[(n + 1) / 2 - 1] = Complex(-1.0, 0.0) # pure real value at exp(i*pi)
		end
		zero_s = []
		gain_s = (c**n).to_f;

		## S-plane frequency transform
		zero_s, pole_s, gain_s = Roctave.sftrans(zero_s, pole_s, gain_s, w, stop)

		## Use bilinear transform to convert poles to the Z plane
		zero_z, pole_z, gain_z = Roctave.bilinear(zero_s, pole_s, gain_s, t)

		## Convert to the correct output form
		b = Roctave.poly(zero_z).collect{|v| (gain_z * v).real}
		a = Roctave.poly(pole_z).collect{|v| v.real}

		return [b, a]
	end
end


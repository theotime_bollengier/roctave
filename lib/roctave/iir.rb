## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	class IirFilter
		include Roctave::Filter

		# @see Roctave.freqz
		def freqz (*args)
			Roctave.freqz(numerator, denominator, *args)
		end

		# Draw the impulse response.
		# @param len [Integer] the length of the displayed response
		# @see Roctave.stem
		def impulse_response (len = 100, *args)
			x = Array.new([1, len.to_i].max){0.0}
			x[0] = 1.0
			y = self.clone.filter x
			Roctave.stem(y, *args)
		end

		# Draw the step response.
		# @param len [Integer] the length of the displayed response
		# @see Roctave.stem
		def step_response (len = 100, *args)
			x = Array.new([1, len.to_i].max){1.0}
			y = self.clone.filter x
			Roctave.stem(y, *args)
		end

		# @see Roctave.zplane
		def zplane
			Roctave.zplane(numerator, denominator)
		end

		# @return [Roctave::IirFilter]
		def clone
			Roctave::IirFilter.new numerator, denominator
		end

		# Returns a Butterworth IIR filter
		# @see Roctave.butter
		# @return [Roctave::IirFilter]
		def self.butter (*args)
			Roctave::IirFilter.new *Roctave.butter(*args)
		end

		# Returns a Chebyshev type I IIR filter
		# @see Roctave.cheby1
		# @return [Roctave::IirFilter]
		def self.cheby1 (*args)
			Roctave::IirFilter.new *Roctave.cheby1(*args)
		end

		# Returns a Chebyshev type II IIR filter
		# @see Roctave.cheby2
		# @return [Roctave::IirFilter]
		def self.cheby2 (*args)
			Roctave::IirFilter.new *Roctave.cheby2(*args)
		end
	end
end



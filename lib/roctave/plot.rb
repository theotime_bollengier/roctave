## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


#begin
	require 'gnuplot'

	module Roctave

		# @!group Plotting

		##
		# Used internally to parse plot() arguments
		def self.parse_plot_args (args)
			datasets = []
			cur_dataset = nil
			state = :x_or_y

			args.each.with_index do |arg, arg_index|
				case state
				when :x_or_y
					unless cur_dataset.nil? then
						if cur_dataset[:y].nil? then
							cur_dataset[:y] = cur_dataset[:x]
							cur_dataset[:x] = (0 ... cur_dataset[:y].length).to_a
						end
						datasets << cur_dataset 
						cur_dataset = nil
					end
					raise ArgumentError.new "Argument #{arg_index + 1} is expected to be an array, either X or Y" unless arg.kind_of?(Enumerable)
					cur_dataset = {x: arg}
					state = :y_or_fmt_or_property
				when :y_or_fmt_or_property
					if arg.kind_of?(Enumerable) then
						cur_dataset[:y] = arg
						raise ArgumentError.new "Arguments #{arg_index} and #{arg_index + 1}: arrays must have the same length" unless cur_dataset[:y].length == cur_dataset[:x].length
						state = :fmt_or_property_or_x
					elsif arg.kind_of?(String) then
						cur_dataset[:y] = cur_dataset[:x]
						cur_dataset[:x] = (0 ... cur_dataset[:y].length).to_a
						state = :fmt_or_property_or_x
						redo
					elsif arg.kind_of?(Symbol) then
						cur_dataset[:y] = cur_dataset[:x]
						cur_dataset[:x] = (0 ... cur_dataset[:y].length).to_a
						state = :property_or_x
						redo
					else
						raise ArgumentError.new "Argument #{arg_index + 1} is expected to be the Y array or the FMT string"
					end
				when :fmt_or_property_or_x
					if arg.kind_of?(Enumerable) then
						state = :x_or_y
						redo
					elsif arg.kind_of?(Symbol) then
						state = :property_or_x
						redo
					elsif arg.kind_of?(String) then
						m = arg.match(/;([^;]+);/)
						if m then
							cur_dataset[:legend] = m[1]
							arg.sub!(/;([^;]+);/, '')
						end

						if arg =~ /--/ then
							cur_dataset[:linetype] = :dashed; arg.sub!(/--/, '')
						elsif arg =~ /-\./ then
							cur_dataset[:linetype] = :dashdotted; arg.sub!(/-\./, '')
						elsif arg =~ /-/ then
							cur_dataset[:linetype] = :solid; arg.sub!(/-/, '')
						elsif arg =~ /:/ then
							cur_dataset[:linetype] = :dotted; arg.sub!(/:/, '')
						end

						if arg =~ /\+/ then
							cur_dataset[:pointtype] = :crosshair; arg.sub!(/\+/, '')
						elsif arg =~ /o/ then
							cur_dataset[:pointtype] = :circle; arg.sub!(/o/, '')
						elsif arg =~ /\*/ then
							cur_dataset[:pointtype] = :star; arg.sub!(/\*/, '')
						elsif arg =~ /\./ then
							cur_dataset[:pointtype] = :point; arg.sub!(/\./, '')
						elsif arg =~ /x/ then
							cur_dataset[:pointtype] = :cross; arg.sub!(/x/, '')
						elsif arg =~ /s/ then
							cur_dataset[:pointtype] = :square; arg.sub!(/s/, '')
						elsif arg =~ /d/ then
							cur_dataset[:pointtype] = :diamond; arg.sub!(/d/, '')
						elsif arg =~ /\^/ then
							cur_dataset[:pointtype] = :upwardFacingTriangle; arg.sub!(/\^/, '')
						elsif arg =~ /v/ then
							cur_dataset[:pointtype] = :downwardFacingTriangle; arg.sub!(/v/, '')
						elsif arg =~ />/ then
							cur_dataset[:pointtype] = :rightFacingTriangle; arg.sub!(/>/, '')
						elsif arg =~ /</ then
							cur_dataset[:pointtype] = :leftFacingTriangle; arg.sub!(/</, '')
						elsif arg =~ /p/ then
							cur_dataset[:pointtype] = :pentagram; arg.sub!(/p/, '')
						elsif arg =~ /h/ then
							cur_dataset[:pointtype] = :hexagram; arg.sub!(/h/, '')
						end

						if arg =~ /r/ then
							cur_dataset[:color] = :red; arg.sub!(/r/, '')
						elsif arg =~ /g/ then
							cur_dataset[:color] = :green; arg.sub!(/g/, '')
						elsif arg =~ /b/ then
							cur_dataset[:color] = :blue; arg.sub!(/b/, '')
						elsif arg =~ /c/ then
							cur_dataset[:color] = :cyan; arg.sub!(/c/, '')
						elsif arg =~ /m/ then
							cur_dataset[:color] = :magenta; arg.sub!(/m/, '')
						elsif arg =~ /y/ then
							cur_dataset[:color] = :yellow; arg.sub!(/y/, '')
						elsif arg =~ /k/ then
							cur_dataset[:color] = :black; arg.sub!(/k/, '')
						elsif arg =~ /w/ then
							cur_dataset[:color] = :white; arg.sub!(/w/, '')
						end


						raise ArgumentError.new "Argument #{arg_index + 1}: cannot parse \"#{arg}\"" unless arg.empty?

						state = :property_or_x
					else
						raise ArgumentError.new "Argument #{arg_index + 1} is expected to be the X array or the FMT string"
					end
				when :property_or_x
					if arg.kind_of?(Enumerable) then
						state = :x_or_y
						redo
					elsif arg.kind_of?(Symbol) then
						case arg
						when :linetype
							state = :prop_linetype
						when :linewidth
							state = :prop_linewidth
						when :dashtype
							state = :prop_dashtype
						when :color
							state = :prop_color
						when :pointtype
							state = :prop_pointtype
						when :pointsize
							state = :prop_pointsize
							#when :pointcolor
							#	state = :prop_pointcolor
						when :legend
							state = :prop_legend
						when :filled
							cur_dataset[:empty] = false
							state = :property_or_x
						when :empty
							cur_dataset[:empty] = true
							state = :property_or_x
						else
							raise RuntimeError.new "Unexpected FSM state (#{state}) while parsing argument #{arg_index+1}, sorry =("
						end
					else
						raise ArgumentError.new "Argument #{arg_index + 1} is expected to be the X array or a property symbol"
					end
				when :prop_linetype
					case arg
					when '--'
						cur_dataset[:linetype] = :dashed
					when :dashed
						cur_dataset[:linetype] = :dashed
					when '-.'
						cur_dataset[:linetype] = :dashdotted
					when :dashdotted
						cur_dataset[:linetype] = :dashdotted
					when '-'
						cur_dataset[:linetype] = :solid
					when :solid
						cur_dataset[:linetype] = :solid
					when ':'
						cur_dataset[:linetype] = :dotted
					when :dotted
						cur_dataset[:linetype] = :dotted
					when :none
						cur_dataset[:linetype] = false
					else
						raise ArgumentError.new "Argument #{arg_index + 1} (linetype) is expected to be either '--', '-.', '-', ':', :dashed, :dashdotted, :solid, :doted"
					end
					state = :property_or_x
				when :prop_linewidth
					raise ArgumentError.new "Argument #{arg_index + 1} (linewidth) is expected to be a numeric value" unless arg.kind_of?(Numeric)
					cur_dataset[:linewidth] = arg.to_f
					state = :property_or_x
				when :prop_dashtype
					raise ArgumentError.new "Argument #{arg_index + 1} (dashtype) is expected to be a string containing only [-._ ]" if not(arg.kind_of?(String)) or arg =~ /[^-\._ ]/
					cur_dataset[:dashtype] = arg
					state = :property_or_x
				when :prop_color
					case arg
					when 'r'
						cur_dataset[:color] = :red
					when 'g'
						cur_dataset[:color] = :green
					when 'b'
						cur_dataset[:color] = :blue
					when 'c'
						cur_dataset[:color] = :cyan
					when 'm'
						cur_dataset[:color] = :magenta
					when 'y'
						cur_dataset[:color] = :yellow
					when 'k'
						cur_dataset[:color] = :black
					when 'w'
						cur_dataset[:color] = :white
					when String
						cur_dataset[:color] = arg.to_sym
					when Symbol
						cur_dataset[:color] = arg
					else
						raise ArgumentError.new "Argument #{arg_index + 1} (color) unexpected \"#{arg}\""
					end
					state = :property_or_x
				when :prop_pointtype
					case arg
					when '+'
						cur_dataset[:pointtype] = :crosshair
					when :crosshair
						cur_dataset[:pointtype] = :crosshair
					when 'o'
						cur_dataset[:pointtype] = :circle
					when :circle
						cur_dataset[:pointtype] = :circle
					when '*'
						cur_dataset[:pointtype] = :star
					when :star
						cur_dataset[:pointtype] = :star
					when '.'
						cur_dataset[:pointtype] = :point
					when :point
						cur_dataset[:pointtype] = :point
					when 'x'
						cur_dataset[:pointtype] = :cross
					when :cross
						cur_dataset[:pointtype] = :cross
					when 's'
						cur_dataset[:pointtype] = :square
					when :square
						cur_dataset[:pointtype] = :square
					when 'd'
						cur_dataset[:pointtype] = :diamond
					when :diamond
						cur_dataset[:pointtype] = :diamond
					when '^'
						cur_dataset[:pointtype] = :upwardFacingTriangle
					when :upwardFacingTriangle
						cur_dataset[:pointtype] = :upwardFacingTriangle
					when 'v'
						cur_dataset[:pointtype] = :downwardFacingTriangle
					when :downwardFacingTriangle
						cur_dataset[:pointtype] = :downwardFacingTriangle
					when '>'
						cur_dataset[:pointtype] = :rightFacingTriangle
					when :rightFacingTriangle
						cur_dataset[:pointtype] = :rightFacingTriangle
					when '<'
						cur_dataset[:pointtype] = :leftFacingTriangle
					when :leftFacingTriangle
						cur_dataset[:pointtype] = :leftFacingTriangle
					when 'p'
						cur_dataset[:pointtype] = :pentagram
					when :pentagram
						cur_dataset[:pointtype] = :pentagram
					when 'h'
						cur_dataset[:pointtype] = :hexagram
					when :hexagram
						cur_dataset[:pointtype] = :hexagram
					when :none
						cur_dataset[:pointtype] = false
					else
						raise ArgumentError.new "Argument #{arg_index + 1} (pointtype) unexpected \"#{arg}\""
					end
					state = :property_or_x
				when :prop_pointsize
					raise ArgumentError.new "Argument #{arg_index + 1} (pointsize) is expected to be a numeric value" unless arg.kind_of?(Numeric)
					cur_dataset[:pointsize] = arg.to_f
					state = :property_or_x
					#when :prop_pointcolor
					#	case arg
					#	when 'k'
					#		cur_dataset[:pointcolor] = :black
					#	when 'r'
					#		cur_dataset[:pointcolor] = :red
					#	when 'g'
					#		cur_dataset[:pointcolor] = :green
					#	when 'b'
					#		cur_dataset[:pointcolor] = :blue
					#	when 'm'
					#		cur_dataset[:pointcolor] = :magenta
					#	when 'c'
					#		cur_dataset[:pointcolor] = :cyan
					#	when 'w'
					#		cur_dataset[:pointcolor] = :white
					#	when String
					#		cur_dataset[:pointcolor] = arg.to_sym
					#	when Symbol
					#		cur_dataset[:pointcolor] = arg
					#	else
					#		raise ArgumentError.new "Argument #{arg_index + 1} (pointcolor) unexpected \"#{arg}\""
					#	end
					#	state = :property_or_x
				when :prop_legend
					raise ArgumentError.new "Argument #{arg_index + 1} (legend) is expected to be a string" unless arg.kind_of?(String)
					cur_dataset[:legend] = arg
					state = :property_or_x
				else
					raise RuntimeError.new "Unexpected FSM state (#{state}) while parsing argument #{arg_index+1}, sorry =("
				end
			end
			unless cur_dataset.nil? then
				if cur_dataset[:y].nil? then
					cur_dataset[:y] = cur_dataset[:x]
					cur_dataset[:x] = (0 ... cur_dataset[:y].length).to_a
				end
				datasets << cur_dataset 
			end
			datasets
		end


		# @param args [Array, String] plot(Y), plot(X, Y), plot(X, Y, FMT), plot(X1, Y1, ..., Xn, Yn). FMT: Linestyle: '-' solid, '--' dashed', ':' dotted, '-.' dash-dotted. Marker: '+' crosshair, 'o' circle, '*' star, '*' star, '.' point, 'x' cross, 's' square, 'd' diamond, '^' upward-facing triangle, 'v' downward-facing triangle, '>' right-facing triangle, '<' left-facing triangle, 'p' pentagram, 'h' hexagram. Color: 'k' blacK, 'r' Red, 'g' Green, 'b' Blue, 'm' Magenta, 'c' Cyan, 'w' White, ";displayname;"
		# @param title [String] Graph title
		# @param xlabel [String] X axix label
		# @param ylabel [String] X axix label
		# @param xlim [Array<Float, Float>, Range] Two element array of the leftmost and rightmost X axis limits
		# @param ylim [Array<Float, Float>, Range] Two element array of the lower and upper Y axis limits
		# @param logx [Integer] Enable log scaling of the X axis. The given number is the base. 0 if for 10
		# @param logy [Integer] Enable log scaling of the Y axis. The given number is the base. 0 if for 10
		# @param grid [Boolean] Display the grid
		# @param terminal [String] The command you would issue to gnuplot to set the terminal, without the 'set terminal ', ex: 'png transparent enhanced'
		# @param output [String] The path of an output file if the terminal allows it.
		def self.plot (*args, title: nil, xlabel: nil, ylabel: nil, xlim: nil, ylim: nil, logx: nil, logy: nil, grid: true, terminal: 'wxt enhanced persist', output: nil, **opts)
			datasets = Roctave.parse_plot_args(args)
			return nil if datasets.empty?

			Gnuplot.open do |gp|
				Gnuplot::Plot.new(gp) do |plot|
					if terminal then
						plot.terminal terminal.to_s
						plot.output output.to_s if output
					end
					plot.title title if title.kind_of?(String)
					plot.xlabel xlabel if xlabel.kind_of?(String)
					plot.ylabel ylabel if ylabel.kind_of?(String)
					plot.xrange "[#{xlim.first}:#{xlim.last}]" if xlim.kind_of?(Array) and xlim.length == 2 and xlim.first.kind_of?(Numeric) and xlim.last.kind_of?(Numeric)
					plot.xrange "[#{xlim.begin}:#{xlim.end}]" if xlim.kind_of?(Range) and xlim.begin.kind_of?(Numeric) and xlim.end.kind_of?(Numeric)
					plot.yrange "[#{ylim.first}:#{ylim.last}]" if ylim.kind_of?(Array) and ylim.length == 2 and ylim.first.kind_of?(Numeric) and ylim.last.kind_of?(Numeric)
					plot.yrange "[#{ylim.begin}:#{ylim.end}]" if ylim.kind_of?(Range) and ylim.begin.kind_of?(Numeric) and ylim.end.kind_of?(Numeric)
					plot.logscale "x#{(logx.to_i > 0) ? " #{logx.to_i}" : ''}" if logx
					plot.logscale "y#{(logy.to_i > 0) ? " #{logy.to_i}" : ''}" if logy

					## Matlab colors ##
					plot.linetype "1 lc rgb '#0072BD'"
					plot.linetype "2 lc rgb '#D95319'"
					plot.linetype "3 lc rgb '#EDB120'"
					plot.linetype "4 lc rgb '#7E2F8E'"
					plot.linetype "5 lc rgb '#77AC30'"
					plot.linetype "6 lc rgb '#4DBEEE'"
					plot.linetype "7 lc rgb '#A2142F'"
					plot.linetype "192 dt solid lc rgb '#df000000'"

					if grid == true then
						plot.grid 'ls 192'
					elsif grid then
						plot.grid grid.to_s
					end

					opts.each do |k, v|
						plot.send(k.to_sym, v.to_s)
					end

					plot.data = datasets.collect do |ds|
						Gnuplot::DataSet.new([ds[:x], ds[:y]]) { |gpds|
							if ds[:legend].kind_of?(String) then
								gpds.title = ds[:legend]
							else
								gpds.notitle
							end
							gpds.linecolor = "'#{ds[:color]}'" if ds[:color]
							gpds.linewidth = "#{ds[:linewidth]}" if ds[:linewidth]

							with = ''
							if (ds[:linetype] or ds[:dashtype] or (ds[:pointtype].nil? and ds[:pointsize].nil?)) then
								with += 'lines' 
								linetype = ''
							else
								linetype = nil
							end
							if ds[:pointtype] or ds[:pointsize] then
								with += 'points' 
								pointtype = ''
							else
								pointtype = nil
							end

							if ds[:linetype] == :dashed then
								linetype += ' dt "-"'
							elsif ds[:linetype] == :dotted then
								linetype += ' dt "."'
							elsif ds[:linetype] == :dashdotted then
								linetype += ' dt "_. "'
							end

							case ds[:pointtype]
							when :crosshair
								pointtype += ' pt 1'
							when :circle
								if ds[:empty] == false then
									pointtype += ' pt 7' # filled
								else
									pointtype += ' pt 6' # emtpy
								end
							when :star
								pointtype += ' pt 3'
							when :point
								if ds[:empty] then
									pointtype += ' pt 6' # emtpy
								else
									pointtype += ' pt 7' # filled
								end
							when :cross
								pointtype += ' pt 2'
							when :square
								if ds[:empty] then
									pointtype += ' pt 4' # emtpy
								else
									pointtype += ' pt 5' # filled
								end
							when :diamond
								if ds[:empty] then
									pointtype += ' pt 12' # emtpy
								else
									pointtype += ' pt 13' # filled
								end
							when :upwardFacingTriangle
								if ds[:empty] then
									pointtype += ' pt 8' # emtpy
								else
									pointtype += ' pt 9' # filled
								end
							when :downwardFacingTriangle
								if ds[:empty] then
									pointtype += ' pt 10' # emtpy
								else
									pointtype += ' pt 11' # filled
								end
							when :rightFacingTriangle
								if ds[:empty] == false then
									pointtype += ' pt 9' # filled
								else
									pointtype += ' pt 8' # emtpy
								end
							when :leftFacingTriangle
								if ds[:empty] == false then
									pointtype += ' pt 11' # filled
								else
									pointtype += ' pt 10' # emtpy
								end
							when :pentagram
								if ds[:empty] then
									pointtype += ' pt 14' # emtpy
								else
									pointtype += ' pt 15' # filled
								end
							when :hexagram
								if ds[:empty] == false then
									pointtype += ' pt 15' # filled
								else
									pointtype += ' pt 14' # emtpy
								end
							end

							unless (linetype.nil? or ds[:dashtype].nil?) then
								linetype.gsub!(/ dt "[-\._ ]"/, '') if linetype =~ / dt "[-\._ ]"/
								linetype += " dt \"#{ds[:dashtype]}\""
							end
							pointtype += " ps #{ds[:pointsize]}" if (pointtype and ds[:pointsize])

							with += linetype if linetype
							with += pointtype if pointtype
							gpds.with = with
						}
					end
				end
			end
		end


		# @param args [Array, String] plot(Y), plot(X, Y), plot(X, Y, FMT), plot(X1, Y1, ..., Xn, Yn). FMT: Linestyle: '-' solid, '--' dashed', ':' dotted, '-.' dash-dotted. Marker: '+' crosshair, 'o' circle, '*' star, '*' star, '.' point, 'x' cross, 's' square, 'd' diamond, '^' upward-facing triangle, 'v' downward-facing triangle, '>' right-facing triangle, '<' left-facing triangle, 'p' pentagram, 'h' hexagram. Color: 'k' blacK, 'r' Red, 'g' Green, 'b' Blue, 'm' Magenta, 'c' Cyan, 'w' White, ";displayname;"
		# @param title [String] Graph title
		# @param xlabel [String] X axix label
		# @param ylabel [String] X axix label
		# @param xlim [Array<Float, Float>, Range] Two element array of the leftmost and rightmost X axis limits
		# @param ylim [Array<Float, Float>, Range] Two element array of the lower and upper Y axis limits
		# @param logx [Integer] Enable log scaling of the X axis. The given number is the base. 0 if for 10
		# @param logy [Integer] Enable log scaling of the Y axis. The given number is the base. 0 if for 10
		# @param grid [Boolean] Display the grid
		# @param terminal [String] The command you would issue to gnuplot to set the terminal, without the 'set terminal ', ex: 'png transparent enhanced'
		# @param output [String] The path of an output file if the terminal allows it.
		def self.stem (*args, title: nil, xlabel: nil, ylabel: nil, xlim: nil, ylim: nil, logx: nil, logy: nil, grid: true, terminal: 'wxt enhanced persist', output: nil, **opts)
			datasets = Roctave.parse_plot_args(args)
			return nil if datasets.empty?

			Gnuplot.open do |gp|
				Gnuplot::Plot.new(gp) do |plot|
					if terminal then
						plot.terminal terminal.to_s
						plot.output output.to_s if output
					end
					plot.title title if title.kind_of?(String)
					plot.xlabel xlabel if xlabel.kind_of?(String)
					plot.ylabel ylabel if ylabel.kind_of?(String)
					plot.xrange "[#{xlim.first}:#{xlim.last}]" if xlim.kind_of?(Array) and xlim.length == 2 and xlim.first.kind_of?(Numeric) and xlim.last.kind_of?(Numeric)
					plot.xrange "[#{xlim.begin}:#{xlim.end}]" if xlim.kind_of?(Range) and xlim.begin.kind_of?(Numeric) and xlim.end.kind_of?(Numeric)
					plot.yrange "[#{ylim.first}:#{ylim.last}]" if ylim.kind_of?(Array) and ylim.length == 2 and ylim.first.kind_of?(Numeric) and ylim.last.kind_of?(Numeric)
					plot.yrange "[#{ylim.begin}:#{ylim.end}]" if ylim.kind_of?(Range) and ylim.begin.kind_of?(Numeric) and ylim.end.kind_of?(Numeric)
					plot.logscale "x#{(logx.to_i > 0) ? " #{logx.to_i}" : ''}" if logx
					plot.logscale "y#{(logy.to_i > 0) ? " #{logy.to_i}" : ''}" if logy

					## Matlab colors ##
					plot.linetype "1 lc rgb '#0072BD'"
					plot.linetype "2 lc rgb '#D95319'"
					plot.linetype "3 lc rgb '#EDB120'"
					plot.linetype "4 lc rgb '#7E2F8E'"
					plot.linetype "5 lc rgb '#77AC30'"
					plot.linetype "6 lc rgb '#4DBEEE'"
					plot.linetype "7 lc rgb '#A2142F'"
					plot.linetype "192 dt solid lc rgb '#df000000'"

					if grid == true then
						plot.grid 'ls 192'
					elsif grid then
						plot.grid grid.to_s
					end

					opts.each do |k, v|
						plot.send(k.to_sym, v.to_s)
					end

					linetype_counter = 1

					datasets.each do |ds|
						ds[:linetype] = :solid if ds[:linetype].nil?
						ds[:pointtype] = :circle if ds[:pointtype].nil? or (ds[:pointtype] == false and ds[:linetype] == false)
						unless ds[:linetype] == false then
							plot.data << Gnuplot::DataSet.new([ds[:x], ds[:y]]) { |gpds|
								if ds[:legend].kind_of?(String) and ds[:pointtype] == false then
									gpds.title = ds[:legend]
								else
									gpds.notitle
								end
								gpds.notitle
								gpds.linecolor = "'#{ds[:color]}'" if ds[:color]
								gpds.linewidth = "#{ds[:linewidth]}" if ds[:linewidth]
								with = 'impulses'
								linetype = nil
								if (ds[:linetype] or ds[:dashtype] or (ds[:pointtype].nil? and ds[:pointsize].nil?)) then
									linetype = ''
								end

								with = 'impulses'
								if ds[:linetype] == :dashed then
									linetype += ' dt "-"'
								elsif ds[:linetype] == :dotted then
									linetype += ' dt "."'
								elsif ds[:linetype] == :dashdotted then
									linetype += ' dt "_. "'
								end

								unless (linetype.nil? or ds[:dashtype].nil?) then
									linetype.gsub!(/ dt "[-\._ ]"/, '') if linetype =~ / dt "[-\._ ]"/
									linetype += " dt \"#{ds[:dashtype]}\""
								end
								with += " lt #{linetype_counter}"
								with += linetype if linetype
								gpds.with = with
							}
						end
						unless ds[:pointtype] == false then
							plot.data << Gnuplot::DataSet.new([ds[:x], ds[:y]]) { |gpds|
								if ds[:legend].kind_of?(String) then
									gpds.title = ds[:legend]
								else
									gpds.notitle
								end

								gpds.linecolor = "'#{ds[:color]}'" if ds[:color]
								gpds.linewidth = "#{ds[:linewidth]}" if ds[:linewidth]

								with = 'points'
								if ds[:pointtype] or ds[:pointsize] then
									pointtype = ''
								else
									pointtype = nil
								end

								case ds[:pointtype]
								when :crosshair
									pointtype += ' pt 1'
								when :circle
									if ds[:empty] == false then
										pointtype += ' pt 7' # filled
									else
										pointtype += ' pt 6' # emtpy
									end
								when :star
									pointtype += ' pt 3'
								when :point
									if ds[:empty] then
										pointtype += ' pt 6' # emtpy
									else
										pointtype += ' pt 7' # filled
									end
								when :cross
									pointtype += ' pt 2'
								when :square
									if ds[:empty] then
										pointtype += ' pt 4' # emtpy
									else
										pointtype += ' pt 5' # filled
									end
								when :diamond
									if ds[:empty] then
										pointtype += ' pt 12' # emtpy
									else
										pointtype += ' pt 13' # filled
									end
								when :upwardFacingTriangle
									if ds[:empty] then
										pointtype += ' pt 8' # emtpy
									else
										pointtype += ' pt 9' # filled
									end
								when :downwardFacingTriangle
									if ds[:empty] then
										pointtype += ' pt 10' # emtpy
									else
										pointtype += ' pt 11' # filled
									end
								when :rightFacingTriangle
									if ds[:empty] == false then
										pointtype += ' pt 9' # filled
									else
										pointtype += ' pt 8' # emtpy
									end
								when :leftFacingTriangle
									if ds[:empty] == false then
										pointtype += ' pt 11' # filled
									else
										pointtype += ' pt 10' # emtpy
									end
								when :pentagram
									if ds[:empty] then
										pointtype += ' pt 14' # emtpy
									else
										pointtype += ' pt 15' # filled
									end
								when :hexagram
									if ds[:empty] == false then
										pointtype += ' pt 15' # filled
									else
										pointtype += ' pt 14' # emtpy
									end
								end

								pointtype += " ps #{ds[:pointsize]}" if (pointtype and ds[:pointsize])

								with += " lt #{linetype_counter}"
								with += pointtype if pointtype

								gpds.with = with
							}
						end
						linetype_counter += 1
					end
				end
			end
		end


		##
		# Plot the poles and zeros. The arguments
		# represent filter coefficients (numerator polynomial b and
		# denominator polynomial a).
		#
		# Note that due to the nature of the roots() function, poles and
		# zeros may be displayed as occurring around a circle rather than at
		# a single point.
		# @param b [Array<Numeric>] Filter numerator polynomial coefficients
		# @param a [Array<Numeric>] Filter denominator polynomial coefficients
		def self.zplane (b, a = []) 
			m = b.length - 1
			n = a.length - 1
			b = [1.0] if b.empty?
			a = [1.0] if b.empty?
			z = Roctave.roots(b) + Roctave.zeros(n - m)
			p = Roctave.roots(a) + Roctave.zeros(m - n)

			z_real = z.collect{|v| v.real}
			p_real = p.collect{|v| v.real}
			z_imag = z.collect{|v| v.imag}
			p_imag = p.collect{|v| v.imag}
			reala = z_real + p_real
			imaga = z_imag + p_imag

			xmin = [-1.0, reala.min].min.to_f
			xmax = [ 1.0, reala.max].max.to_f
			ymin = [-1.0, imaga.min].min.to_f
			ymax = [ 1.0, imaga.max].max.to_f
			xfluff = [0.05*(xmax-xmin), (1.05*(ymax-ymin)-(xmax-xmin))/10.0].max
			yfluff = [0.05*(ymax-ymin), (1.05*(xmax-xmin)-(ymax-ymin))/10.0].max
			xmin = xmin - xfluff
			xmax = xmax + xfluff
			ymin = ymin - yfluff
			ymax = ymax + yfluff

			rx = (0..100).collect{|i| Math.cos(2*Math::PI*i/100)}
			ry = (0..100).collect{|i| Math.sin(2*Math::PI*i/100)}

			Roctave.plot(z_real, z_imag, 'o;Zeros;', :pointsize, 1.5, p_real, p_imag, 'x;Poles;', :pointsize, 1.5, rx, ry, '-', :color, '#505050', xlabel: 'Real', ylabel: 'Imaginary', title: '{/:Bold Z-plane}', grid: true, xlim: [xmin, xmax], ylim: [ymin, ymax], size: 'ratio -1')
			nil
		end

	end
#rescue LoadError => e
#	STDERR.puts "WARNING: #{e.message}\nPloting functions will not be available."
#end


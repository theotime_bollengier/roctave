## Copyright (C) 1999 Paul Kienzle <pkienzle@users.sf.net> (GNU Octave implementation)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave

	##
	# Transform a s-plane filter specification into a z-plane
	# specification. Filter is specified in zero-pole-gain form.
	# 1/T is the sampling frequency represented in the z plane.
	#
	# Note: this differs from the bilinear function in the signal processing
	# toolbox, which uses 1/T rather than T.
	#
	# Theory: Given a piecewise flat filter design, you can transform it
	# from the s-plane to the z-plane while maintaining the band edges by
	# means of the bilinear transform.  This maps the left hand side of the
	# s-plane into the interior of the unit circle.  The mapping is highly
	# non-linear, so you must design your filter with band edges in the
	# s-plane positioned at 2/T tan(w*T/2) so that they will be positioned
	# at w after the bilinear transform is complete.
	#
	# Please note that a pole and a zero at the same place exactly cancel.
	# This is significant since the bilinear transform creates numerous
	# extra poles and zeros, most of which cancel. Those which do not
	# cancel have a "fill-in" effect, extending the shorter of the sets to
	# have the same number of as the longer of the sets of poles and zeros
	# (or at least split the difference in the case of the band pass
	# filter). There may be other opportunistic cancellations but I will
	# not check for them.
	#
	# Also note that any pole on the unit circle or beyond will result in
	# an unstable filter.  Because of cancellation, this will only happen
	# if the number of poles is smaller than the number of zeros.  The
	# analytic design methods all yield more poles than zeros, so this will
	# not be a problem.
	#
	# References:
	# Proakis & Manolakis (1992). Digital Signal Processing. New York:
	# Macmillan Publishing Company.
	#
	# @param sz [Array<Numeric>] Zeros in the S-plane
	# @param sp [Array<Numeric>] Poles in the S-plane
	# @param sg [Numeric] Gain in the S-plane
	# @param t [Numeric] The sampling period in the Z-plane
	# @return [Array<Array, Numeric>] An array containing zeros, poles and gain in the Z-plane [zz, zp, zg]
	def self.bilinear (sz, sp, sg, t)
		p = sp.length
		z = sz.length

		if z > p or p == 0 then
			raise ArgumentError.new "bilinear: must have at least as many poles as zeros in s-plane"
		end

		## ----------------  -------------------------  ------------------------ ##
		## Bilinear          zero: (2+xT)/(2-xT)        pole: (2+xT)/(2-xT)      ##
		##      2 z-1        pole: -1                   zero: -1                 ##
		## S -> - ---        gain: (2-xT)/T             gain: (2-xT)/T           ##
		##      T z+1                                                            ##
		## ----------------  -------------------------  ------------------------ ##
		zg = (sg * sz.inject(1.0){|m, v| m * ((2.0-v*t)/t)} / sp.inject(1.0){|m, v| m * ((2.0-v*t)/t)}).real
		zp = sp.collect{|v| (2.0+v*t)/(2.0-v*t)}
		if sz.empty? then
			zz = Array.new(p){-1.0}
		else
			zz = sz.collect{|v| (2.0+v*t)/(2.0-v*t)}
			if zz.length > p then
				zz = zz[0...p]
			elsif zz.length < p then
				zz = zz + Array.new(p - zz.length){-1.0}
			end
		end

		return [zz, zp, zg]
	end
end


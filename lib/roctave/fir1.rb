## Copyright (C) 2000 Paul Kienzle <pkienzle@users.sf.net> (GNU Octave implementation)
## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	# @!group FIR filters

	def self.fir1 (n, w, *args, ramp: nil, window: :hamming)
		raise ArgumentError.new "Expecting no more than 4 arguments" if args.length > 2

		n = [1, n.to_i].max

		if w.kind_of?(Range) then
			w = [w.begin, w.end]
		elsif w.kind_of?(Numeric) then
			w = [w]
		end
		raise ArgumentError.new "W must be a scalar, an array or a range" unless w.kind_of?(Array)
		w.collect! do |e|
			if e.kind_of?(Range) then
				[e.begin, e.end]
			else
				e
			end
		end
		w = w.flatten

		scale = 1
		ftype = (w.length == 1) ? 1 : 0

		args.each do |arg|
			case arg
			when Symbol
				arg = arg.to_s.downcase.to_sym
			when String
				arg = arg.downcase.to_sym
			else
				raise ArgumentError.new "Not expecting a #{arg.class}"
			end

			case arg
			when :low
				ftype = 1
			when :stop
				ftype = 1
			when :dc1
				ftype = 1
			when :high
				ftype = 0
			when :pass
				ftype = 0
			when :bandpass
				ftype = 0
			when :dc0
				ftype = 0
			when :scale
				scale = 1
			when :noscale
				scale = 0
			else
				raise ArgumentError.new "Unexpected argument #{arg}"
			end
		end

		## Build response function according to fir2 requirements
		bands = w.length + 1
		f = Roctave.zeros(2*bands)
		f[0] = 0.0
		f[-1] = 1.0
		w.each.with_index do |e, i|
			j = 1+2*i
			break if j >= 2*bands - 1
			f[j] = e
		end
		w.each.with_index do |e, i|
			j = 2+2*i
			break if j >= 2*bands - 1
			f[j] = e
		end
		m = Roctave.zeros(2*bands)
		r = (1..bands).collect{|e| (e - (1-ftype)) % 2}
		r.each.with_index do |e, i|
			j = 2*i
			break if j >= 2*bands
			m[j] = e
		end
		r = (0...2*bands).step(2).collect{|i| m[i]}
		r.each.with_index do |e, i|
			j = 1+2*i
			break if j >= 2*bands
			m[j] = e
		end

		if (n & 1) == 1 and m[-1].abs > 0.0 then
			raise ArgumentError.new "Cannot do an odd order FIR filter with non-zero magnitude response at the nyquist frequency"
		end

		b = Roctave.fir2(n, f, m, ramp: ramp, window: window)

		if scale == 1 then
			if m[0] == 1 then
				w_o = 0.0
			elsif f[3] == 1 then
				w_o = 1.0
			else
				w_o = f[2] + (f[3] - f[2]) / 2.0
			end
			renorm = 1.0 / b.collect.with_index{ |e, i|
				j = b.length - 1 - i
				e * (Complex.polar(1.0, -Math::PI*w_o)**j)
			}.inject(&:+).abs
			#puts "w_o: #{w_o}, renorm: #{renorm}"
			b.collect!{|e| e * renorm}
		end

		b
	end
end


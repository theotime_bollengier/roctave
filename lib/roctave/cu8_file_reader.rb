## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	class CU8FileReader
		# Read the content of the file as couples of real and imaginary unsigned chars,
		# and return an array of complex numbers whose real and imaginary parts are floating
		# point numbers in the range [-1.0, 1.0]
		# @param filename [String] the path of the file to read
		# @return [Array<Float>] array of complex numbers read from the file
		def self.read (filename)
			reader = Roctave::CU8FileReader.new(filename)
			arr = reader.read
			reader.close
			return arr
		end


		# Creates a new CU8FileReader with the given file name, yields it,
		# then close it and return the result of the block.
		# @param filename [String] the path of the file to read
		def self.open (filename)
			reader = Roctave::CU8FileReader.new(filename)
			res = yield(reader)
			reader.close
			return res
		end
	end
end


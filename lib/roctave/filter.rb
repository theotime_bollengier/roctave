## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	module Filter
		# @param flt [Roctave::FirFilter, Roctave::IirFilter]
		# @return [Roctave::FirFilter, Roctave::IirFilter]
		def cascade (flt)
			my_b = numerator
			my_a = denominator

			his_b = flt.numerator
			his_a = flt.denominator

			c = my_b
			d = his_b
			len = c.length + d.length - 1
			d = len.times.collect do |i|
				([0, i - d.length + 1].max .. [c.length - 1, i].min).inject(0.0){|sum, k| sum + c[k]*d[i-k]}
			end
			b = d

			c = my_a
			d = his_a
			len = c.length + d.length - 1
			d = len.times.collect do |i|
				([0, i - d.length + 1].max .. [c.length - 1, i].min).inject(0.0){|sum, k| sum + c[k]*d[i-k]}
			end
			a = d

			b.collect!{|v| v/ a[0]}
			a.collect!{|v| v/ a[0]}

			fir = true
			a[1..-1].each do |v|
					fir = false if v != 0.0
			end

			if fir then
				r = Roctave::FirFilter.new(b)
			else
				r = Roctave::IirFilter.new(b, a)
			end

			r
		end
	end
end


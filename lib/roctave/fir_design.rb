## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	# @!group FIR filters

	##
	# @param n [Integer] The filter order
	# @param f [Float] Cutoff frequency in the range [0 1] maping to w = [0 pi]
	# @param window [Array<Float>, Symbol] The window to use, either an array of length N+1, or a symbol. Defaults to :hamming
	# @return [Array<Float>] The filter coefficients B
	def self.fir_low_pass (n, f, window: :hamming)
		f = [0.0, [1.0, f.to_f].min].max
		n = [1, n.to_i].max
		len = n + 1

		case window
		when Array
			raise ArgumentError.new "Window array must be of length #{len}" unless window.length == len
		when Symbol
			window = self.send(window, len)
		when nil
		else
			raise ArgumentError.new "Window must either be an array of length N+1 or the symbol of a window"
		end

		f *= Math::PI
		b = (0...len).collect do |i|
			j = i - n/2.0
			if j == 0.0 then
				f / Math::PI
			else
				Math.sin(f*j) / (Math::PI*j)
			end
		end

		b.collect!.with_index{|e, i| e*window[i]} if window

		b
	end


	##
	# @param n [Integer] The filter order, must be even
	# @param f [Float] Cutoff frequency in the range [0 1] maping to w = [0 pi]
	# @param window [Array<Float>, Symbol] The window to use, either an array of length N+1, or a symbol. Defaults to :hamming
	# @return [Array<Float>] The filter coefficients B
	def self.fir_high_pass (n, f, window: :hamming)
		f = [0.0, [1.0, f.to_f].min].max
		n = [1, n.to_i].max
		if (n & 1) != 0 then
			n += 1
			STDERR.puts "WARNING: #{self.class}::#{__method__}() Order must be even, incrementing to #{n}"
		end
		len = n + 1

		case window
		when Array
			raise ArgumentError.new "Window array must be of length #{len}" unless window.length == len
		when Symbol
			window = self.send(window, len)
		when nil
		else
			raise ArgumentError.new "Window must either be an array of length N+1 or the symbol of a window"
		end

		f *= Math::PI
		b = (0...len).collect do |i|
			j = i - n/2.0
			if j == 0.0 then
				1.0 - f / Math::PI
			else
				-Math.sin(f*j) / (Math::PI*j)
			end
		end

		b.collect!.with_index{|e, i| e*window[i]} if window

		b
	end


	##
	# @param n [Integer] The filter order, must be even
	# @param f [Float, Array<Float, Float>, Range] Either a two elements array or a range defining the band edges, or the center frequency (in that case +width+ must be specified). Frequencies must be the range [0 1] maping to w = [0 pi]
	# @param width [Float] Width around the ceter frequency +f+, maping to w = [0 pi]
	# @param window [Array<Float>, Symbol] The window to use, either an array of length N+1, or a symbol. Defaults to :hamming
	# @return [Array<Float>] The filter coefficients B
	def self.fir_band_stop (n, f, width = nil, window: :hamming)
		if f.kind_of?(Array) then
			raise ArgumentError.new "Second element must be a two element array containing band edges or the center frequency" unless f.length == 2
			f1 = f.first
			f2 = f.last
			raise ArgumentError.new "Width around the center frequeny is not used when band edges are specified with an array" unless width.nil?
		elsif f.kind_of?(Range) then
			f1 = f.begin
			f2 = f.end
			raise ArgumentError.new "Width around the center frequeny is not used when band edges are specified with an range" unless width.nil?
		elsif f.kind_of?(Numeric) then
			raise ArgumentError.new "You must specify the Width around the center frequeny when the center frequency is specified as a scalar" unless width.kind_of?(Float)
			f = [0.0, [1.0, f.to_f].min].max
			width = [0.0, [2.0, width.to_f].min].max / 2.0
			f1 = f - width
			f2 = f + width
		else
			raise ArgumentError.new "Second argument must be a scalar, an array or a range"
		end

		f1 = [0.0, [1.0, f1.to_f].min].max
		f2 = [0.0, [1.0, f2.to_f].min].max
		f1,f2 = f2,f1 if f1 > f2
		n = [1, n.to_i].max
		if (n & 1) != 0 then
			n += 1
			STDERR.puts "WARNING: #{self.class}::#{__method__}() Order must be even, incrementing to #{n}"
		end
		len = n + 1

		case window
		when Array
			raise ArgumentError.new "Window array must be of length #{len}" unless window.length == len
		when Symbol
			window = self.send(window, len)
		when nil
		else
			raise ArgumentError.new "Window must either be an array of length N+1 or the symbol of a window"
		end

		bl = Roctave.fir_low_pass(n, f1, window: window)
		bh = Roctave.fir_high_pass(n, f2, window: window)

		(0...len).collect{|i| bl[i] + bh[i]}
	end


	##
	# @param n [Integer] The filter order, must be even
	# @param f [Float, Array<Float, Float>, Range] Either a two elements array or a range defining the band edges, or the center frequency (in that case +width+ must be specified). Frequencies must be the range [0 1] maping to w = [0 pi]
	# @param width [Float] Width around the ceter frequency +f+, maping to w = [0 pi]
	# @param window [Array<Float>, Symbol] The window to use, either an array of length N+1, or a symbol. Defaults to :hamming
	# @return [Array<Float>] The filter coefficients B
	def self.fir_band_pass (n, f, width = nil, window: :hamming)
		if f.kind_of?(Array) then
			raise ArgumentError.new "Second element must be a two element array containing band edges or the center frequency" unless f.length == 2
			f1 = f.first
			f2 = f.last
			raise ArgumentError.new "Width around the center frequeny is not used when band edges are specified with an array" unless width.nil?
		elsif f.kind_of?(Range) then
			f1 = f.begin
			f2 = f.end
			raise ArgumentError.new "Width around the center frequeny is not used when band edges are specified with an range" unless width.nil?
		elsif f.kind_of?(Numeric) then
			raise ArgumentError.new "You must specify the Width around the center frequeny when the center frequency is specified as a scalar" unless width.kind_of?(Float)
			f = [0.0, [1.0, f.to_f].min].max
			width = [0.0, [2.0, width.to_f].min].max / 2.0
			f1 = f - width
			f2 = f + width
		else
			raise ArgumentError.new "Second argument must be a scalar, an array or a range"
		end

		f1 = [0.0, [1.0, f1.to_f].min].max
		f2 = [0.0, [1.0, f2.to_f].min].max
		f1,f2 = f2,f1 if f1 > f2
		n = [1, n.to_i].max
		parity = ((n & 1) == 0) ? :even : :odd
		len = n + 1

		case window
		when Array
			raise ArgumentError.new "Window array must be of length #{len}" unless window.length == len
		when Symbol
			window = self.send(window, len)
		when nil
		else
			raise ArgumentError.new "Window must either be an array of length N+1 or the symbol of a window"
		end

		if parity == :even then
			b = Roctave.fir_band_stop(n, [f1, f2], window: window)
			b.collect!.with_index do |e, i|
				r = -e
				r += 1 if i == n / 2.0
				r
			end
		else
			bl = Roctave.fir_low_pass(n, f1, window: window)
			bh = Roctave.fir_low_pass(n, f2, window: window)

			b = (0...len).collect{|i| bh[i] - bl[i]}
		end

		b
	end


	def self.fir_differentiator (n, window: :blackman)
		n = [2, n.to_i].max
		if (n & 1) != 0 then
			n += 1
			STDERR.puts "WARNING: #{self.name}::#{__method__}() Forcing type III FIR filter, order must be even, incrementing to #{n}."
		end
		len = n + 1

		case window
		when Array
			raise ArgumentError.new "Window array must be of length #{len}" unless window.length == len
		when Symbol
			window = self.send(window, len)
		when nil
		else
			raise ArgumentError.new "Window must either be an array of length N+1 or the symbol of a window"
		end

		b = (-n/2 .. n/2).collect do |i|
			if i == 0 then
				0.0
			else
				(-1)**i / i.to_f
			end
		end

		b.collect!.with_index{|e, i| e*window[i]} if window

		b
	end


	def self.fir_hilbert (n, window: :hamming)
		n = [2, n.to_i].max
		if (n & 1) != 0 then
			n += 1
			STDERR.puts "WARNING: #{self.name}::#{__method__}() Forcing type III FIR filter, order must be even, incrementing to #{n}."
		end
		len = n + 1

		case window
		when Array
			raise ArgumentError.new "Window array must be of length #{len}" unless window.length == len
		when Symbol
			window = self.send(window, len)
		when nil
		else
			raise ArgumentError.new "Window must either be an array of length N+1 or the symbol of a window"
		end

		b = (-n/2 .. n/2).collect do |i|
			if i == 0 then
				0.0
			else
				(1 - (-1)**i) / (Math::PI * i)
			end
		end

		b.collect!.with_index{|e, i| e*window[i]} if window

		b
	end


	##
	# Not very usefull, not better than fir_differentiator()
	# @param fc [Float] Normalized cutoff frequency in the range [0 0.5]
	def self.fir_differentiator_low_pass (n, fc = 0.5, window: :hamming)
		fc = [0.0, [0.5, fc.to_f].min].max
		n = [2, n.to_i].max
		if (n & 1) != 0 then
			n += 1
			STDERR.puts "WARNING: #{self.name}::#{__method__}() Forcing type III FIR filter, order must be even, incrementing to #{n}."
		end
		len = n + 1

		case window
		when Array
			raise ArgumentError.new "Window array must be of length #{len}" unless window.length == len
		when Symbol
			window = self.send(window, len)
		when nil
		else
			raise ArgumentError.new "Window must either be an array of length N+1 or the symbol of a window"
		end

		b = (-n/2 .. n/2).collect do |i|
			if i == 0 then
				0.0
			else
				-2.0/(Math::PI*i*i)*Math.sin(Math::PI*i*fc)*(1.0 - Math.cos(Math::PI*i*fc))
			end
		end

		b.collect!.with_index{|e, i| e*window[i]} if window

		b
	end
end


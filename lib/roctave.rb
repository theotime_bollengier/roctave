## Copyright (C) 2019 Théotime Bollengier <theotime.bollengier@gmail.com>
##
## This file is part of Roctave
## 
## Roctave is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## Roctave is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with Roctave. If not, see <https://www.gnu.org/licenses/>.


module Roctave
	##
	# Return a row vector with +n+ linearly spaced elements between +base+
    # and +limit+.
	# @overload linspace(base, limit, nb_elem)
	#  @param base [Numeric]
	#  @param limit [Numeric]
	#  @param nb_elem [Integer] number of elements
	#  @return [Array<Numeric>]
	# @overload linspace(range, nb_elem)
	#  @param range [Range]
	#  @param nb_elem [Integer] number of elements
	#  @return [Array<Numeric>]
	def self.linspace (base, limit = nil, nb_elem)
		nb_elem = [1, nb_elem.to_i].max
		if limit.nil? then
			limit = base.end
			exclude = base.exclude_end?
			base = base.begin
			limit -= (limit - base) / nb_elem.to_f if exclude
		end
		return [base] if nb_elem == 1
		nm1 = (nb_elem - 1).to_f
		(0...nb_elem).collect do |i|
			base + i*(limit - base) / nm1
		end
	end


	# @param n [Integer] the lenght of the array
	# @return [Array<Float>]
	def self.zeros (len)
		len = [0, len.to_i].max
		Array.new(len){0.0}
	end


	# @param n [Integer] the lenght of the array
	# @return [Array<Float>]
	def self.ones (len)
		len = [0, len.to_i].max
		Array.new(len){1.0}
	end

	##
	# Unwrap radian phases by adding multiples of 2*pi as appropriate to
	# remove jumps greater than TOL.
	# 
	# TOL defaults to pi.
	# @param x [Array<Float>] Input
	# @param tol [Float] Threshold detection level
	# @return [Array<Float>]
	def self.unwrap (x, tol = Math::PI)
		raise ArgumentError.new "First argument must be an array" unless x.kind_of?(Array)
		tol = tol.to_f.abs
		rng = 2.0*Math::PI
		d = (1...x.length).collect{|i| x[i] - x[i-1]}
		p = d.collect do |e| 
			v = (e.abs / rng).ceil * rng
			if e > tol then
				-v
			elsif e < -tol then
				v
			else
				0.0
			end
		end
		r = Array.new(x.length){0.0}
		p.each.with_index do |e, i|
			r[i+1] = r[i] + e
		end
		r.collect.with_index{|e, i| e + x[i]}
	end


	##
	# Append the scalar value C to the vector X until it is of length L.
	# If C is not given, a value of 0 is used.
	# 
	# If 'length (X) > L', elements from the end of X are removed until a
	# vector of length L is obtained.<Paste>
	# @param x [Array<Numeric>] Input array
	# @param l [Integer] The requested length
	# @param c [Numeric] Value to pad with (default: 0.0)
	def self.postpad(x, l, c = 0.0)
		raise ArgumentError.new "First argument must be an array" unless x.kind_of?(Array)
		l = [0, l.to_i].max
		len = x.length
		if l == len then
			x = x.collect{|v| v}
		elsif l < len then
			x = x[0...l]
		else
			x = x + Array.new(l - len){c}
		end
		x
	end

end


class Array
	def abs
		self.collect(&:abs)
	end

	def real
		self.collect(&:real)
	end

	def imag
		self.collect(&:imag)
	end

	def arg
		#self.collect{|e| e.kind_of?(Complex) ? e.arg : Complex(e).arg}
		self.collect(&:arg)
	end

	def conj
		self.collect(&:conj)
	end
end

require 'matrix'
require_relative 'roctave/version.rb'
require_relative 'roctave/roctave'
require_relative 'roctave/window.rb'
require_relative 'roctave/dft.rb'
require_relative 'roctave/roots.rb'
require_relative 'roctave/plot.rb'
require_relative 'roctave/freqz.rb'
require_relative 'roctave/interp1.rb'
require_relative 'roctave/fir2.rb'
require_relative 'roctave/fir1.rb'
require_relative 'roctave/firls.rb'
require_relative 'roctave/firpm.rb'
require_relative 'roctave/fir_design.rb'
require_relative 'roctave/finite_difference_coefficients.rb'
require_relative 'roctave/sftrans.rb'
require_relative 'roctave/bilinear.rb'
require_relative 'roctave/poly.rb'
require_relative 'roctave/butter.rb'
require_relative 'roctave/cheby.rb'
require_relative 'roctave/filter.rb'
require_relative 'roctave/fir.rb'
require_relative 'roctave/iir.rb'
require_relative 'roctave/cu8_file_reader.rb'
require_relative 'roctave/freq_shifter.rb'

